package higherground.lib.satpaqusbdriver;

import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.util.Log;

import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialPort;
import com.hoho.android.usbserial.util.SerialInputOutputManager;

import java.io.IOException;
import java.util.concurrent.Executors;

class UsbSerialSocket implements SerialInputOutputManager.Listener {
    private static final String TAG = "higherground.lib.satpaqusbdriver.UsbSerialSocket";
    private enum UsbPermission { Unknown, Requested, Granted, Denied }

    private static final String INTENT_ACTION_GRANT_USB = BuildConfig.LIBRARY_PACKAGE_NAME + ".GRANT_USB";
    private static final String INTENT_ACTION_DISCONNECT = BuildConfig.LIBRARY_PACKAGE_NAME + ".Disconnect";
    private static final int WRITE_WAIT_MILLIS = 2000;

    private BroadcastReceiver broadcastReceiver;
    //private Handler mainLooper;
    private final Context context;

    private SerialInputOutputManager ioManager = null;
    private UsbSerialPort serialPort = null;
    private UsbPermission usbPermission = UsbPermission.Unknown;
    private UsbDeviceConnection usbConnection = null;
    //private boolean connected = false;

    // Callback system
    private final SerialListener listener;

    @SuppressLint("LongLogTag")
    UsbSerialSocket(Context context, SerialListener listener) {
        Log.d(TAG, "constructor");
        this.context = context;
        this.listener = listener;

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.i(TAG, "broadcastReceiver.onReceive()");
                if ((intent != null) && (intent.getAction() != null)) {
                    if (intent.getAction().equals(INTENT_ACTION_GRANT_USB)) {
                        usbPermission = intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)
                                ? UsbPermission.Granted : UsbPermission.Denied;

                        // Deregister the Receiver.  This should get set back up again for the
                        // disconnect Intent during the connect();
                        context.unregisterReceiver(broadcastReceiver);

                        if (usbPermission == UsbPermission.Granted) {
                            try {
                                connect();
                            } catch (Exception e) {
                                Log.e(TAG, "connect exception: " + e.toString());
                            }
                        }
                    } else if (intent.getAction().equals(INTENT_ACTION_DISCONNECT)) {
                        // Run the disconnect() method to clean up our variables.
                        disconnect();
                    } else {
                        Log.d(TAG, "unknown intent: " + intent.toString());
                    }
                } else {
                    Log.e(TAG, "BroadcastReceiver: onReceive: null Intent or Intent action");
                }
            }
        };
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onRunError(Exception e) {
        Log.i(TAG, "onRunError: " + e.toString());
        listener.onSerialIoError(e);
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onNewData(byte[] data) {
        Log.i(TAG,"onNewData: " + new String(data));
        listener.onSerialRead(data);
    }

    @SuppressLint("LongLogTag")
    void connect() {
        Log.d(TAG, "connect()");

        if (this.serialPort != null) {
            Log.e(TAG, "serialPort is not null meaning it is already connected.");
            listener.onSerialConnectError(new IOException("already connected"));
        }

        // Detect our usbDevices
        UsbDevice device = null;
        UsbSerialDriver driver = null;
        UsbManager usbManager = (UsbManager) context.getSystemService(Context.USB_SERVICE);
        if(usbManager == null) {
            Log.e(TAG, "connect(): new UsbManager: returned null");
            listener.onSerialConnectError(new IOException("Unable to connect to UsbManager"));
            return;
        }

        for (UsbDevice v : usbManager.getDeviceList().values()) {
            Log.d(TAG, "connect(): getDeviceList: " + v.getDeviceId());

            // See if it is a device we support.
            driver = SupportedDeviceProber.getCustomProber().probeDevice(v);
            if (driver == null) {
                Log.e(TAG, "unable to get driver for device: " + v.toString());
            } else {
                device = v;
                break;
            }
        }
        if (device == null) {
            Log.e(TAG, "connect(): device not found");
            listener.onSerialConnectError(new IOException("Device not found"));
            return;
        }

        Log.d(TAG, "UsbDevice: " + device.toString());

        // Check to make sure that I have permission before trying to open the device.
        if(!usbManager.hasPermission(driver.getDevice())) {
            Log.d(TAG, "No permission to connect to device.  Requesting permission.");

            // Register a receiver for granting permission.
            context.registerReceiver(broadcastReceiver, new IntentFilter(INTENT_ACTION_GRANT_USB));

            PendingIntent usbPermissionIntent = PendingIntent.getBroadcast(context, 0, new Intent(INTENT_ACTION_GRANT_USB), 0);
            usbManager.requestPermission(driver.getDevice(), usbPermissionIntent);
            usbPermission = UsbPermission.Requested;

            return;
        }

        usbConnection = usbManager.openDevice(driver.getDevice());
        if(usbConnection == null) {
            Log.e(TAG, "Unable to get connection to device");
            listener.onSerialConnectError(new IOException("Unable to open device"));

            return;
        }

        // It appears that we're always looking for port 0.
        Log.d(TAG, "ports: " + driver.getPorts().toString());
        serialPort = driver.getPorts().get(0);

        // Now that all the formalities have been taken care of, register a listener before
        // opening the port.
        // XXX - Does this get leaked if not reaped?
        context.registerReceiver(broadcastReceiver, new IntentFilter(INTENT_ACTION_DISCONNECT));

        // Note: This is synchronous
        try {
            serialPort.open(usbConnection);
            serialPort.setParameters(115200, UsbSerialPort.DATABITS_8, UsbSerialPort.STOPBITS_1, UsbSerialPort.PARITY_NONE);
            // These may or may not be necessary:
            serialPort.setDTR(true);
            serialPort.setRTS(true);
        } catch(Exception e) {
            listener.onSerialConnectError(e);
            serialPort = null;

            return;
        }

        ioManager = new SerialInputOutputManager(serialPort, this);
        Executors.newSingleThreadExecutor().submit(ioManager);

        listener.onSerialConnect();
    }

    @SuppressLint("LongLogTag")
    void disconnect() {
        Log.d(TAG, "disconnect()");
        if(ioManager != null) {
            Log.d(TAG, "stopping ioManager");
            ioManager.setListener(null);
            ioManager.stop();
            ioManager = null;
        }
        if(serialPort != null) {
            Log.d(TAG, "closing serial port");
            try {
                serialPort.setDTR(false);
                serialPort.setRTS(false);
                serialPort.close();
            } catch (Exception e) {
                Log.e(TAG, "disconnect exception:" + e.toString());
            }

            serialPort = null;
        }
        if(usbConnection != null) {
            Log.d(TAG, "closing USB connection.");
            usbConnection.close();
            usbConnection = null;
        }
        listener.onSerialDisconnect();
    }

    @SuppressLint("LongLogTag")
    UsbReturnCodes write(byte[] data) {
        Log.d(TAG, "write()");
        if(serialPort == null) {
            Log.e(TAG, "write: disconnected");
            return UsbReturnCodes.DISCONNECTED;
        }

        try {
            serialPort.write(data, WRITE_WAIT_MILLIS);
        } catch (IOException e) {
            Log.e(TAG, "IO exception: " + e.toString());

            return UsbReturnCodes.IO_ERROR;
        } catch (Exception e) {
            Log.e(TAG, "write exception: " + e.toString());

            return UsbReturnCodes.SEND_FAIL;
        }

        Log.i(TAG,"write: successful");
        return UsbReturnCodes.OKAY;
    }
}
