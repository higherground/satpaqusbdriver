package higherground.lib.satpaqusbdriver;

import higherground.lib.satpaqusbdriver.satpaq.EndpointType;
import higherground.lib.satpaqusbdriver.satpaq.enums.GattCharacteristic;
import higherground.lib.satpaqusbdriver.satpaq.enums.GattOperation;
import higherground.lib.satpaqusbdriver.satpaq.enums.GattService;

public class SatPaqEndpoint extends UsbMessage {
    public EndpointType type;
    public GattService service;
    public GattCharacteristic characteristic;
    public GattOperation operation;
    public int msgID;
    public byte[] data;
}
