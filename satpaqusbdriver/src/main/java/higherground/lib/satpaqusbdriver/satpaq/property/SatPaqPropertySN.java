package higherground.lib.satpaqusbdriver.satpaq.property;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;

import higherground.lib.satpaqusbdriver.satpaq.enums.HGNordicCommandType;
import higherground.lib.satpaqusbdriver.utils.HGStringUtil;

public class SatPaqPropertySN extends AbstractSatPaqProperty {
    private final static int SATPAQ_SN_MAX_LEN = 16;

    private String m_serialNumber;

    public SatPaqPropertySN() {
        super(HGNordicCommandType.CMD_SN_GET.getValue());
    }

    public SatPaqPropertySN(byte[] data) {
        super(data);

        if(m_propertyData != null && m_propertyData.length > 0) {
            m_serialNumber = new String(m_propertyData, Charset.defaultCharset());
        } else {
            m_serialNumber = "";
        }
    }

    public SatPaqPropertySN(String serialNumber) {
        super(HGNordicCommandType.CMD_SN_SET.getValue());
        m_serialNumber = serialNumber;
    }

    public String getSN() {
        return m_serialNumber;
    }

    @Override
    public byte[] getCommandData() {
        return getStandardGetCommandData();
    }

    @Override
    public byte[] setCommandData() {
        if(HGStringUtil.length(m_serialNumber) > SATPAQ_SN_MAX_LEN) {
            m_serialNumber = m_serialNumber.substring(0, SATPAQ_SN_MAX_LEN);
        }

        byte[] cmdData = new byte[1 + HGStringUtil.length(m_serialNumber)];
        ByteBuffer buffer = ByteBuffer.wrap(cmdData);

        buffer = buffer.order(ByteOrder.LITTLE_ENDIAN);
        buffer.put(m_commandType);
        buffer.put(m_serialNumber.getBytes(Charset.defaultCharset()));

        return buffer.array();
    }
}
