package higherground.lib.satpaqusbdriver.satpaq.property;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;

import higherground.lib.satpaqusbdriver.satpaq.enums.HGNordicCommandType;
import higherground.lib.satpaqusbdriver.utils.HGStringUtil;

public class SatPaqPropertyLogData extends AbstractSatPaqProperty {
    private final static int SATPAQ_LOG_COMMAND_MAX = 20;

    byte logPath;
    byte buffered;

    String command;

    public SatPaqPropertyLogData() {
        super(HGNordicCommandType.CMD_LOG_DATA.getValue());
    }

    public SatPaqPropertyLogData(byte[] data) {
        super(data);
    }

    @Override
    public byte[] setCommandData() {
        ByteBuffer buffer = null;
        HGNordicCommandType commandType = HGNordicCommandType.fromInt(m_commandType);
        switch(commandType) {
            case CMD_LOG_START: {
                byte[] cmdData = new byte[3];
                buffer = ByteBuffer.wrap(cmdData);
                buffer = buffer.order(ByteOrder.LITTLE_ENDIAN);
                buffer.put(m_commandType);
                buffer.put(logPath);
                buffer.put(buffered);
                break;
            }
            case CMD_LOG_STOP:
            case CMD_LOG_DUMP: {
                byte[] cmdData = new byte[1];
                buffer = ByteBuffer.wrap(cmdData);
                buffer = buffer.order(ByteOrder.LITTLE_ENDIAN);
                buffer.put(m_commandType);
                break;
            }
            case CMD_LOG_COMMAND: {
                if(HGStringUtil.length(command) > SATPAQ_LOG_COMMAND_MAX) {
                    command = command.substring(0, SATPAQ_LOG_COMMAND_MAX);
                }

                byte[] cmdData = new byte[1 + HGStringUtil.length(command)];
                buffer = ByteBuffer.wrap(cmdData);

                buffer = buffer.order(ByteOrder.LITTLE_ENDIAN);
                buffer.putInt(m_commandType);
                buffer.put(command.getBytes(Charset.defaultCharset()));
                break;
            }
            default:
                break;
        }

        if(buffer != null) {
            return buffer.array();
        } else {
            return null;
        }

    }

    @Override
    public byte[] getCommandData() {
        return getStandardGetCommandData();
    }
}
