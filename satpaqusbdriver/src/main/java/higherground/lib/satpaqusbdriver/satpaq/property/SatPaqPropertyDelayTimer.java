package higherground.lib.satpaqusbdriver.satpaq.property;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import higherground.lib.satpaqusbdriver.satpaq.enums.HGNordicCommandType;

public class SatPaqPropertyDelayTimer extends AbstractSatPaqProperty {
    private final static int SATPAQ_NAME_MAX = 16;

    private byte timer_index;
    private byte control;
    private int delay;

    public SatPaqPropertyDelayTimer() {
        super(HGNordicCommandType.CMD_DELAY_TIMER.getValue());
    }

    public SatPaqPropertyDelayTimer(byte[] data) {
        super(data);

        ByteBuffer buffer = ByteBuffer.wrap(m_propertyData);
        buffer = buffer.order(ByteOrder.LITTLE_ENDIAN);

        timer_index = buffer.get();
        control = buffer.get();
    }

    public byte getTimer_index() {
        return timer_index;
    }

    public byte getControl() {
        return control;
    }

    public int getDelay() {
        return delay;
    }

    public void setTimer_index(byte timer_index) {
        this.timer_index = timer_index;
    }

    public void setControl(byte control) {
        this.control = control;
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }

    @Override
    public byte[] setCommandData() {
        byte[] cmdData = new byte[35];
        ByteBuffer buffer = ByteBuffer.wrap(cmdData);

        buffer = buffer.order(ByteOrder.LITTLE_ENDIAN);
        buffer.put(m_commandType);
        buffer.put(timer_index);
        buffer.put(control);
        buffer.putInt(delay);

        return buffer.array();
    }

    @Override
    public byte[] getCommandData() {
        return getStandardGetCommandData();
    }
}
