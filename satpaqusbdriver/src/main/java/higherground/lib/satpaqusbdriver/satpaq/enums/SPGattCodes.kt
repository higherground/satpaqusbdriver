package higherground.lib.satpaqusbdriver.satpaq.enums

/**
 * Gatt Service and Characteristic definitions.
 * Used internally and for ncmd address by SatPaq USB
 * NOTE: Keep this in sync with the SatPaq Firmware definitions
 * See: SatPaq Monitor User's Guide
 * https://docs.google.com/document/d/1GvSSDdd1OoxwkZg9tkI9D847lBvt1mQnZQPD1ECLHAk/edit#heading=h.ycvas6yq4tf0
 */

enum class GattService(val service: Byte) {
    DeviceInfo(0),          // (Standard) Device Information Service
    Battery(1),             // (Standard) Battery Service
    SatPaq(2)               // (Custom) Satpaq Service
}

enum class GattCharacteristic(val characteristic: Byte, val service: GattService) {
    // Device Characteristics
    // TODO: Revisit
    ManufactureName(0, GattService.DeviceInfo),
    ModelNumber(1, GattService.DeviceInfo),
    SerialNumber(2, GattService.DeviceInfo),
    HardwareRevision(3, GattService.DeviceInfo),
    FirmwareRevision(4, GattService.DeviceInfo),
    SoftwareRevision(5, GattService.DeviceInfo),
    SystemID(6, GattService.DeviceInfo),
    BoardRevision(7, GattService.DeviceInfo),

    // Battery Characteristics
    BatteryLevel(0, GattService.Battery),    // Battery charge level 0..100
    BatteryStatus(1, GattService.Battery),   // TODO: Does this really exist??

    // SatPaq Characteristics
    Command(0, GattService.SatPaq),
    Response(1, GattService.SatPaq),
    Status(2, GattService.SatPaq),
    RXData(3, GattService.SatPaq),      // Only used by BLE
    TXData(4, GattService.SatPaq),      // Only used by BLE
}

enum class GattOperation(val characteristic: Byte) {
    ReadProperty(0),
    WriteProperty(1),
    ReadValue(2),
    WriteValue(3),
}