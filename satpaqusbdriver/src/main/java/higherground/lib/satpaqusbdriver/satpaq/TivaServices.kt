package higherground.lib.satpaqusbdriver.satpaq

import android.util.Log
import higherground.lib.satpaqusbdriver.satpaq.operations.*
import higherground.lib.satpaqusbdriver.utils.HGJsonUtil

object TivaServices {
    private const val LOG_TAG = "TivaServices"

    fun parse(epType: EndpointType, response: String) : SPOperation {
        return when (epType) {
            EndpointType.TivaCommand -> parseMonitorResponse(response)
            else -> parseUnsupported()
        }
    }

    private fun parseUnsupported(): SPOperation {
        //HGNormalLog.error(LOG_TAG, "parseUnsupported: Unsupported PTClient type ${ptcp.type}")
        // Should not reach here
        assert(false)
        return SPOperation(opType = SPOpType.UNDEFINED)
    }

    private fun parseMonitorResponse(response: String): SPOperation {
        when (HGJsonUtil.getFirstKey(response)) {
            "sver" -> return parseTivaInfo(response)
            "jxlog" -> return parseJXLog(response)
            "app" -> return parseApp(response)
            "uecho" -> return parseUecho(response)
        }

        return SPOperation(opType = SPOpType.UNDEFINED)
    }

    private fun parseUecho(response: String): SPOperation {
        val op: SPOperation = TivaUEcho.factory(response)
        // Alex TODO - I should not need to do this assignment...
        op.opType = SPOpType.TIVA_UECHO
        return op
    }

    private fun parseTivaInfo(response: String): SPOperation {
        Log.e(LOG_TAG, "parseTivaInfo")
        val op: SPOperation = TivaInfo.getSatPaqTivaInfo(response)
        // Alex TODO - I should not need to do this assignment...
        op.opType = SPOpType.TIVA_INFO
        return op
    }

    private fun parseJXLog(response: String): SPOperation {
        val op: SPOperation = TivaJXLog.factory(response)
        // Alex TODO - I should not need to do this assignment...
        op.opType = SPOpType.TIVA_JXLOG
        return op
    }

    private fun parseApp(response: String): SPOperation {
        val op: SPOperation = TivaApp.factory(response)
        // Alex TODO - I should not need to do this assignment...
        op.opType = SPOpType.TIVA_APP
        return op
    }
}