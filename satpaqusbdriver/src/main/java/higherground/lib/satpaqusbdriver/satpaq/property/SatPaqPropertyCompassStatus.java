package higherground.lib.satpaqusbdriver.satpaq.property;

import higherground.lib.satpaqusbdriver.satpaq.enums.HGNordicCommandType;

public class SatPaqPropertyCompassStatus extends AbstractSatPaqProperty {

    public SatPaqPropertyCompassStatus() {
        super(HGNordicCommandType.CMD_COMPASS_STATUS_GET.getValue());
    }

    public SatPaqPropertyCompassStatus(byte[] data) {
        super(data);
    }

    @Override
    public byte[] setCommandData() {
        return new byte[0];
    }

    @Override
    public byte[] getCommandData() {
        return new byte[0];
    }
}
