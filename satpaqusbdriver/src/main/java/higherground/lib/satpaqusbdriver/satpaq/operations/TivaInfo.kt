package higherground.lib.satpaqusbdriver.satpaq.operations

import com.google.gson.GsonBuilder
import com.google.gson.annotations.SerializedName
import higherground.lib.satpaqusbdriver.utils.HGVersionUtil

/** Represents information about the TIVA chip in a SatPaq.  */
class TivaInfo : SPOperation {
    enum class HGSatPaqTivaInfoResult {
        Succeeded, BleError, BadResponse
    }

    @SerializedName("sver")
    var data: SVerData? = null

    inner class SVerData {
        @SerializedName("id")
        var identifier: String? = null
        @SerializedName("hw")
        var hardwareVersion: String? = null
        @SerializedName("FSP00-00001")
        var bootloader: SVerDetail? = null
        @SerializedName("FSP00-00002")
        var firmware: SVerDetail? = null
        @SerializedName("sn")
        var serialNumber: String? = null
    }

    inner class SVerDetail {
        @SerializedName("version")
        var version: String? = null
        @SerializedName("description")
        var description: String? = null
        @SerializedName("built")
        var built: String? = null
    }

    // Alex TODO - this does not seem to intialize the optype
    constructor(sverString: String) : super(opType = SPOpType.TIVA_INFO) {
        getSatPaqTivaInfo(sverString)
    }

    constructor() : super(opType = SPOpType.TIVA_INFO) {

    }

    interface HGSatPaqTivaInfoCallback {
        fun tivaInfoLoaded(result: HGSatPaqTivaInfoResult?, satPaqTivaInfo: TivaInfo?)
    }

    fun serialize(): String {
        val json = GsonBuilder().create()
        return json.toJson(this)
    }

    val isSupportReturnEsN0: Boolean
        get() {
            val version: String = this.data?.firmware?.version ?: return true
            return HGVersionUtil.getInstance().compare(version, RETURN_ESN0_SUPPORT_TIVA_VER) >= 0
        }

    /**
     * Returns whether the TIVA supports the "fwdconfig.epochs_per_symbol" setting.
     *
     * @see  [
     * The design doc](https://docs.google.com/document/d/1hx4Lu6HPb1NdJwV4lX-AzrqcpHuTbvk3MS_5v4oMWsM) for details.
     */
    fun supportsEpochsPerSymbol(): Boolean {
        val version: String = this.data?.firmware?.version ?: return false
        return HGVersionUtil.getInstance().compare(
                version, FIRST_FWD_PATH_EPOCHS_PER_SYMBOL_TIVA_VER) >= 0
    }

    companion object {
        /**
         * The "fwdconfig.epochs_per_symbol" value old TIVA firmware implicitly assumes.
         * As of 2020-04-09, this value corresponds to full rate forward paths.
         *
         * @see  [
         * The design doc](https://docs.google.com/document/d/1hx4Lu6HPb1NdJwV4lX-AzrqcpHuTbvk3MS_5v4oMWsM) for details.
         */
        const val OLD_TIVA_FWD_PATH_EPOCHS_PER_SYMBOL = 3
        private const val FIRST_FWD_PATH_EPOCHS_PER_SYMBOL_TIVA_VER = "2.10.0"
        private const val RETURN_ESN0_SUPPORT_TIVA_VER = "2.8.4"
        @JvmStatic
        fun getSatPaqTivaInfo(serializedString: String?): TivaInfo {
            val json = GsonBuilder().create()
            return json.fromJson(serializedString, TivaInfo::class.java)
        }
    }
}
