package higherground.lib.satpaqusbdriver.satpaq.property;

import higherground.lib.satpaqusbdriver.satpaq.enums.HGNordicCommandType;

public class SatPaqPropertyConfig extends AbstractSatPaqProperty {
    public SatPaqPropertyConfig() {
        super(HGNordicCommandType.CMD_CONFIG_PARAM_GET.getValue());
    }

    public SatPaqPropertyConfig(byte[] data) {
        super(data);
    }

    @Override
    public byte[] setCommandData() {
        return new byte[0];
    }

    @Override
    public byte[] getCommandData() {
        return new byte[0];
    }
}
