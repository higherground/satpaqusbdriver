package higherground.lib.satpaqusbdriver.satpaq.property;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import higherground.lib.satpaqusbdriver.satpaq.enums.HGNordicCommandType;

public class SatPaqPropertyAccel extends AbstractSatPaqProperty {
    private final static int SATPAQ_NAME_MAX = 16;

    private int x;
    private int y;
    private int z;

    public SatPaqPropertyAccel() {
        super(HGNordicCommandType.CMD_ACCEL_GET.getValue());
    }

    public SatPaqPropertyAccel(byte[] data) {
        super(data);

        ByteBuffer buffer = ByteBuffer.wrap(m_propertyData);
        buffer = buffer.order(ByteOrder.LITTLE_ENDIAN);

        x = buffer.getShort();
        y = buffer.getShort();
        z = buffer.getShort();
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }

    @Override
    public byte[] setCommandData() {
        return null;
    }

    @Override
    public byte[] getCommandData() {
        return getStandardGetCommandData();
    }
}
