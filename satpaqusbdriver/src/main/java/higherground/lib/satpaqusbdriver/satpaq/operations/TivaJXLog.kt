package higherground.lib.satpaqusbdriver.satpaq.operations

import com.google.gson.GsonBuilder
import com.google.gson.annotations.SerializedName

class TivaJXLog : SPOperation {
    @SerializedName("jxlog")
    var data: String = ""

    // Alex TODO - this does not seem to intialize the optype
    constructor(jsonStr: String) : super(opType = SPOpType.TIVA_JXLOG) {
        factory(jsonStr)
    }

    companion object {
        @JvmStatic
        fun factory(serializedString: String?): TivaJXLog {
            val json = GsonBuilder().create()
            return json.fromJson(serializedString, TivaJXLog::class.java)
        }
    }
}

