package higherground.lib.satpaqusbdriver.satpaq.property;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;

import higherground.lib.satpaqusbdriver.satpaq.enums.HGNordicCommandType;
import higherground.lib.satpaqusbdriver.utils.HGStringUtil;

public class SatPaqPropertyName extends AbstractSatPaqProperty {
    private final static int SATPAQ_NAME_MAX = 16;

    private String m_name;

    public SatPaqPropertyName() {
        super(HGNordicCommandType.CMD_SATPAQ_NAME_GET.getValue());
    }

    public SatPaqPropertyName(byte[] data) {
        super(data);

        m_name = new String(m_propertyData);
    }

    public SatPaqPropertyName(String name) {
        super(HGNordicCommandType.CMD_SATPAQ_NAME_SET.getValue());

        m_name = name;
    }

    public String getName() {
        return m_name;
    }

    @Override
    public byte[] setCommandData() {
        if(HGStringUtil.length(m_name) > SATPAQ_NAME_MAX) {
            m_name = m_name.substring(0, SATPAQ_NAME_MAX);
        }

        byte[] cmdData = new byte[1 + HGStringUtil.length(m_name)];
        ByteBuffer buffer = ByteBuffer.wrap(cmdData);

        buffer = buffer.order(ByteOrder.LITTLE_ENDIAN);
        buffer.put(m_commandType);
        buffer.put(m_name.getBytes(Charset.defaultCharset()));

        return buffer.array();
    }

    @Override
    public byte[] getCommandData() {
        return getStandardGetCommandData();
    }
}
