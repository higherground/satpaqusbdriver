package higherground.lib.satpaqusbdriver.satpaq.enums

// TODO: Add unit tests so all the warnings suppressed below naturally go away.
@Suppress("unused")
enum class SatPaqConnectionState(val text: String) {
    DISCONNECTED("Disconnected"),
    SCANNING("Scanning"),
    CONNECTING("Connecting"),
    CONNECTED("Connected"),
    DISCONNECTING("Disconnecting"),
    FAILED("Failed"),
    DELETED("Deleted"),
    DFU_NEEDED("Dfu Needed")
}
