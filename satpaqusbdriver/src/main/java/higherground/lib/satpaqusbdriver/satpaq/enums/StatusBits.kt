package higherground.lib.satpaqusbdriver.satpaq.enums

import higherground.lib.satpaqusbdriver.utils.HasRawValue

enum class StatusBits(override val rawValue: Int): HasRawValue {
    Docked(1),
    Charging(2),
    TivaAwake(4),
    TooHot(8)
}
