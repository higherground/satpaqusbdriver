package higherground.lib.satpaqusbdriver.satpaq.property;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import higherground.lib.satpaqusbdriver.satpaq.enums.HGNordicCommandType;

public class SatPaqPropertyTemperature extends AbstractSatPaqProperty {

    private int temperature;

    public SatPaqPropertyTemperature() {
        super(HGNordicCommandType.CMD_NORDIC_TEMP_GET.getValue());
    }

    public SatPaqPropertyTemperature(byte[] data) {
        super(data);

        ByteBuffer buffer = ByteBuffer.wrap(m_propertyData);
        buffer = buffer.order(ByteOrder.LITTLE_ENDIAN);

        float tempVal = (float)buffer.getInt() / (float)4;
        tempVal = tempVal * 1.8f + 32.0f;

        temperature = (int)tempVal;
    }

    public int getTemperature() {
        return temperature;
    }

    @Override
    public byte[] setCommandData() {
        return null;
    }

    @Override
    public byte[] getCommandData() {
        return getStandardGetCommandData();
    }
}
