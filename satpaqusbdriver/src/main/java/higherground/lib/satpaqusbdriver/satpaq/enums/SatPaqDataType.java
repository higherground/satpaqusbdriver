package higherground.lib.satpaqusbdriver.satpaq.enums;

// TODO Should this be converted to an enum class?
public class SatPaqDataType {
    /**
     * a
     */
    public static final byte ECHO_REQUEST = (byte) 'a';
    /**
     * A
     */
    public static final byte ECHO_RESPONSE = (byte) 'A';
    /**
     * m
     */
    public static final byte MONITOR_REQUEST = (byte) 'm';
    /**
     * M: Monitor command response or unsolicited monitor updates
     */
    public static final byte MONITOR_RESPONSE = (byte) 'M';
    /**
     * s: Application Data to Transmit over Satellite
     */
    public static final byte DATA_REQUEST = (byte) 's';
    /**
     * S: Application Data received over Satellite
     */
    public static final byte DATA_RESPONSE = (byte) 'S';
    /**
     * Q
     */
    public static final byte STATUS_RESPONSE = (byte) 'Q';
    /**
     * O
     */
    public static final byte ORIENTATION_RESPONSE = (byte) 'O';
    /**
     * n: Nordic -> TIVA communications
     */
    public static final byte NORDIC_TO_TIVA = (byte) 'n';
    /**
     * N: TIVA -> Nordic communications
     */
    public static final byte TIVA_TO_NORDIC = (byte) 'N';

    public static final byte FW_UPDATE_TRANSMIT = (byte) 'f';


    public static final byte BROADCAST_TYPE = (byte) 0x42;

    public static final byte BCAST_ZFEM = (byte) 0x01;
    public static final byte BCAST_TOD = (byte) 0x02;
    public static final byte BCAST_HCU = (byte) 0x03;

    public static final byte BCAST_PSC = (byte) 0x04;
    public static final byte BCAST_ZFEM2 = (byte) 0x05;
    public static final byte BCAST_HCU2 = (byte) 0x06;
    public static final int BCAST_FRAG_FORMAT_YES = 0b10000000;
}
