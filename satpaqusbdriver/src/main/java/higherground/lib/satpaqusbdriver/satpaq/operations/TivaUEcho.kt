package higherground.lib.satpaqusbdriver.satpaq.operations

import com.google.gson.GsonBuilder
import com.google.gson.annotations.SerializedName

class TivaUEcho : SPOperation {
    @SerializedName("uecho")
    var data: UEcho? = null

    inner class UEcho {
        var vers: String? = null
        var msg: String? = null
    }

    // Alex TODO - this does not seem to intialize the optype
    constructor(jsonStr: String) : super(opType = SPOpType.TIVA_UECHO) {
        factory(jsonStr)
    }

    companion object {
        @JvmStatic
        fun factory(serializedString: String?): TivaUEcho {
            val json = GsonBuilder().create()
            return json.fromJson(serializedString, TivaUEcho::class.java)
        }
    }
}
