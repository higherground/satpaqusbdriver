package higherground.lib.satpaqusbdriver.satpaq.property;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Locale;

import higherground.lib.satpaqusbdriver.satpaq.enums.HGNordicCommandType;

public class SatPaqPropertyCompassRaw extends AbstractSatPaqProperty {
    byte reserved;

    short magVector_x;
    short magVector_y;
    short magVector_z;
    short gyroVector_x;
    short gyroVector_y;
    short gyroVector_z;
    short accelVector_x;
    short accelVector_y;
    short accelVector_z;

    public SatPaqPropertyCompassRaw() {
        super(HGNordicCommandType.CMD_COMPASS_RAW.getValue());
    }

    public SatPaqPropertyCompassRaw(byte[] data) {
        super(data);

        ByteBuffer bb = ByteBuffer.wrap(m_propertyData);
        bb = bb.order(ByteOrder.LITTLE_ENDIAN);

        reserved = bb.get();

        magVector_x = bb.getShort();
        magVector_y = bb.getShort();
        magVector_z = bb.getShort();

        gyroVector_x = bb.getShort();
        gyroVector_y = bb.getShort();
        gyroVector_z = bb.getShort();

        accelVector_x = bb.getShort();
        accelVector_y = bb.getShort();
        accelVector_z = bb.getShort();
    }

    @Override
    public byte[] setCommandData() {
        return new byte[0];
    }

    @Override
    public byte[] getCommandData() {
        return new byte[0];
    }

    public String getLogString() {
        return String.format(Locale.US, "%d, %d, %d, %d, %d, %d, %d, %d, %d, %d",
                m_commandType,
                magVector_x,
                magVector_y,
                magVector_z,
                gyroVector_x,
                gyroVector_y,
                gyroVector_z,
                accelVector_x,
                accelVector_y,
                accelVector_z);
    }
}
