package higherground.lib.satpaqusbdriver.satpaq

import higherground.lib.satpaqusbdriver.satpaq.operations.SPOpType
import higherground.lib.satpaqusbdriver.satpaq.operations.SPOperation
import higherground.lib.satpaqusbdriver.utils.HGBits

/**
 * Singleton to manage SP Operation callbacks.
 * Generally the SatPaq helpers produce the SP Operation events and View Models consume them
 * but it is possible for any class to produce the events.
 */
object SatPaqCallbacks {

    private var observers: MutableMap<String, SPOperation> = mutableMapOf()

    fun callbackRegister(operation: SPOperation) {
        observers[operation.clientID] = operation
    }

    fun callbackDeregister(clientID: String) {
        observers.remove(clientID)
    }

    fun clientCallback(sp: SPOperation) {
        if (sp.opType == SPOpType.UNDEFINED) {
            return
        }

        // We need to iterate over a *copy* of the observers to ensure one shot style callbacks that
        // deregister themselves once they're called are supported. Failing to use a copy will cause
        // a `ConcurrentModificationException` to be thrown otherwise (or would result in strange
        // undefined behavior).
        val observers: List<SPOperation> = this.observers.values.toList()
        for (observer in observers) {
            if (HGBits.isSet(observer.opType, sp.opType)) {
                observer.callback?.invoke(sp)
            }
        }
    }
}
