package higherground.lib.satpaqusbdriver.satpaq.property;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Locale;

import higherground.lib.satpaqusbdriver.satpaq.enums.HGNordicCommandType;

public class SatPaqPropertyCompassDebug extends AbstractSatPaqProperty {
    byte key;

    short debug_1;
    short debug_2;
    short debug_3;
    short debug_4;
    short debug_5;
    short debug_6;
    short debug_7;
    short debug_8;
    short debug_9;

    public SatPaqPropertyCompassDebug() {
        super(HGNordicCommandType.CMD_COMPASS_DEBUG.getValue());
    }

    public SatPaqPropertyCompassDebug(byte[] data) {
        super(data);

        ByteBuffer bb = ByteBuffer.wrap(m_propertyData);
        bb = bb.order(ByteOrder.LITTLE_ENDIAN);

        key = bb.get();
        debug_1 = bb.getShort();
        debug_2 = bb.getShort();
        debug_3 = bb.getShort();
        debug_4 = bb.getShort();
        debug_5 = bb.getShort();
        debug_6 = bb.getShort();
        debug_7 = bb.getShort();
        debug_8 = bb.getShort();
        debug_9 = bb.getShort();
    }

    @Override
    public byte[] setCommandData() {
        return new byte[0];
    }

    @Override
    public byte[] getCommandData() {
        return new byte[0];
    }

    public String getLogString() {
        return String.format(Locale.US, "%d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d",
                m_commandType,
                key,
                debug_1,
                debug_2,
                debug_3,
                debug_4,
                debug_5,
                debug_6,
                debug_7,
                debug_8,
                debug_9);
    }
}
