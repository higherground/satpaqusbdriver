package higherground.lib.satpaqusbdriver.satpaq.property;


import java.util.Arrays;

public abstract class AbstractSatPaqProperty {
    protected byte m_commandType;
    protected byte[] m_propertyData;

    public AbstractSatPaqProperty(byte commandType) {
        m_commandType = commandType;
    }

    public AbstractSatPaqProperty(byte[] propertyData) {
        if(propertyData != null && propertyData.length != 0) {
            m_commandType = propertyData[0];
            m_propertyData = Arrays.copyOfRange(propertyData, 1, propertyData.length);
        }
    }

    public abstract byte[] setCommandData();

    public abstract byte[] getCommandData();

    protected byte[] getStandardGetCommandData() {
        byte[] cmdData = new byte[1];
        cmdData[0] = (byte)m_commandType;
        return cmdData;
    }

    public byte[] getPropertyData() {
        return m_propertyData;
    }
}
