package higherground.lib.satpaq

import higherground.lib.logger.HGNormalLog
import higherground.lib.satpaqusbdriver.satpaq.enums.HGNordicCommandType
import higherground.lib.satpaqusbdriver.satpaq.operations.NordicInfo
import higherground.lib.satpaqusbdriver.satpaq.operations.SPOpType
import higherground.lib.satpaqusbdriver.satpaq.operations.SPOperation
import higherground.lib.satpaqusbdriver.satpaq.enums.GattCharacteristic
import higherground.lib.satpaqusbdriver.satpaq.enums.GattService
import higherground.lib.satpaqusbdriver.utils.HGByteBuffer
import java.nio.ByteOrder

object NordicServices {
    private const val LOG_TAG = "Nordic Services"
    var fooNumber = 0

/*    fun getNordicInfo() :NordicInfo {
        return nordicInfo
    }
*/

    val nordicInfo: NordicInfo = NordicInfo.factory()
        get() {
            return field
        }

    fun parse(service: GattService, characteristic: GattCharacteristic, data: ByteArray): SPOperation {
        return when (service) {
            GattService.DeviceInfo -> parseDeviceInfo(characteristic, data)
            GattService.Battery -> parseBattery(characteristic, data)
            GattService.SatPaq -> parseSatpaq(characteristic, data)
        }
    }

    private fun parseSatpaq(characteristic: GattCharacteristic, data: ByteArray): SPOperation {
        return when (characteristic) {
            GattCharacteristic.Response -> parseResponse(data)
            GattCharacteristic.Status -> parseStatus(data)
            else -> {
                HGNormalLog.error(
                        LOG_TAG,
                        "parseSatpaq: Unsupported SatPaq Characteristic $characteristic")
                parseUnsupported()
            }
        }
    }

    private fun parseStatus(data: ByteArray): SPOperation {
        val newStatus: Int = data[0].toInt()

        return if (nordicInfo.statusUpdate(newStatus)) {
            nordicInfo
        } else {
            SPOperation(opType = SPOpType.UNDEFINED)
        }
    }

    private fun parseResponse(data: ByteArray): SPOperation {
        val num: Int = data[0].toInt()

        return when (val cmd: HGNordicCommandType = HGNordicCommandType.fromInt(num)) {
            HGNordicCommandType.CMD_SN_GET -> parseSerialNumber(data)
            HGNordicCommandType.CMD_JERK_GET -> parseJerk(data)
            HGNordicCommandType.CMD_NORDIC_TEMP_GET -> parseTemperature(data)
            HGNordicCommandType.CMD_SATPAQ_NAME_GET -> parseName(data)
            else -> {
                HGNormalLog.error(
                        LOG_TAG,
                        "parseResponse: Unsupported SatPaq Response Command $cmd")
                parseUnsupported()
            }
        }
    }

    fun parseSerialNumber(data: ByteArray): SPOperation {
        val d : ByteArray = data.copyOfRange(1, data.size)
        val sn: String = String(d)
        nordicInfo.serialNumber = sn
        return nordicInfo
    }

    fun parseName(data: ByteArray): SPOperation {
        val name = String(data.copyOfRange(1, data.size))
        nordicInfo.name = name
        return nordicInfo
    }

    fun parseTemperature(data: ByteArray): SPOperation {
        val bb = HGByteBuffer(data)
        bb.order(ByteOrder.LITTLE_ENDIAN)
        bb.next8()  //Skip command code
        val temperature: Int = bb.next32()
        nordicInfo.temperature = temperature.toDouble() / 4
        return nordicInfo
    }

    fun parseJerk(data: ByteArray): SPOperation {
        val bb = HGByteBuffer(data)
        bb.order(ByteOrder.LITTLE_ENDIAN)
        bb.next8()  //Skip command cod
        nordicInfo.jerk = bb.nextU16()
        return nordicInfo
    }

    private fun parseBattery(characteristic: GattCharacteristic, data: ByteArray): SPOperation {
        if (characteristic == GattCharacteristic.BatteryLevel) {
            val battery: Int = data[0].toInt()
            nordicInfo.batteryLevel = battery
            return nordicInfo
        }

        return parseUnsupported()
    }

    private fun parseDeviceInfo(characteristic: GattCharacteristic, data: ByteArray): SPOperation {
        return when (characteristic) {
            GattCharacteristic.ManufactureName -> parseManufactureName(data)
            GattCharacteristic.ModelNumber -> parseModelNumber(data)
            GattCharacteristic.SerialNumber -> parseAdvertisedName(data)
            GattCharacteristic.HardwareRevision -> parseHWRevision(data)
            GattCharacteristic.FirmwareRevision -> parseFWVersion(data)
            GattCharacteristic.SoftwareRevision -> parseSWRevision(data)
            GattCharacteristic.SystemID -> parseSystemID(data)
            GattCharacteristic.BoardRevision -> parseBoardRevision(data)
            else -> parseUnsupported()
        }
    }

    private fun parseBoardRevision(data: ByteArray): SPOperation {
        val str = String(data)
        nordicInfo.boardRevision = str
        return nordicInfo
    }

    private fun parseSystemID(data: ByteArray): SPOperation {
        val bb = HGByteBuffer(data)
        bb.order(ByteOrder.LITTLE_ENDIAN)
        val num = bb.nextU64()  //Skip command code
        nordicInfo.systemID = num
        return nordicInfo
    }

    private fun parseAdvertisedName(data: ByteArray): SPOperation {
        val str = String(data)
        nordicInfo.advertisedName = str
        return nordicInfo
    }

    private fun parseModelNumber(data: ByteArray): SPOperation {
        val str = String(data)
        nordicInfo.modelNumber = str
        return nordicInfo
    }

    private fun parseSWRevision(data: ByteArray): SPOperation {
        val str = String(data)
        nordicInfo.firmwareRevision = str
        return nordicInfo
    }

    private fun parseHWRevision(data: ByteArray): SPOperation {
        val str = String(data)
        nordicInfo.hardwareRevision = str
        return nordicInfo
    }

    private fun parseManufactureName(data: ByteArray): SPOperation {
        val str = String(data)
        nordicInfo.manufactureName = str
        return nordicInfo
    }

    private fun parseFWVersion(data: ByteArray): SPOperation {
        val str = String(data)
        nordicInfo.bootloaderRevision = str
        return nordicInfo
    }

    private fun parseUnsupported(): SPOperation {
        // Should not reach here
        assert(false)

        return SPOperation(opType = SPOpType.UNDEFINED)
    }
}
