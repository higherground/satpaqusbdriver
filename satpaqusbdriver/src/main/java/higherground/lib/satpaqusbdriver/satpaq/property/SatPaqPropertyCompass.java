package higherground.lib.satpaqusbdriver.satpaq.property;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Locale;

import higherground.lib.satpaqusbdriver.satpaq.enums.HGNordicCommandType;

public class SatPaqPropertyCompass extends AbstractSatPaqProperty {
    private final static int COMPASS_START_CONTROL_TARGET       = 0x01;
    private final static int COMPASS_START_CONTROL_STANDARD     = 0x02;
    private final static int COMPASS_START_CONTROL_RAW          = 0x04;
    private final static int COMPASS_START_CONTROL_DEBUG_1      = 0x08;
    private final static int COMPASS_START_CONTROL_DEBUG_2      = 0x10;
    private final static int COMPASS_START_CONTROL_DEBUG_3      = 0x20;
    private final static int COMPASS_START_CONTROL_SOFTIRON     = 0x40;

    byte m_control;
    short m_distance;
    short m_tar;
    byte m_par;
    byte m_tae;
    byte m_reserved1;
    byte m_tme;
    byte m_reserved2;
    short m_orientingRate;
    short m_orientedRate;

    int compassIdleTime;

    public SatPaqPropertyCompass() {
        super(HGNordicCommandType.CMD_COMPASS_START.getValue());
    }

    public SatPaqPropertyCompass(boolean target,
                                 boolean normal,
                                 boolean raw,
                                 boolean debug1,
                                 boolean debug2,
                                 boolean debug3,
                                 short distance,
                                 byte tae,
                                 byte tme,
                                 short orientingRate,
                                 short orientedRate) {

        super(HGNordicCommandType.CMD_COMPASS_START.getValue());

        m_control = 0;

        if(target) {
            m_control |= COMPASS_START_CONTROL_TARGET;
        }

        if(normal) {
            m_control |= COMPASS_START_CONTROL_STANDARD;
        }

        if(raw) {
            m_control |= COMPASS_START_CONTROL_RAW;
        }

        if(debug1) {
            m_control |= COMPASS_START_CONTROL_DEBUG_1;
        }

        if(debug2) {
            m_control |= COMPASS_START_CONTROL_DEBUG_2;
        }

        if(debug3) {
            m_control |= COMPASS_START_CONTROL_DEBUG_3;
        }

        m_distance = distance;
        m_tae = tae;
        m_tme = tme;
        m_orientingRate = orientingRate;
        m_orientedRate = orientedRate;
    }

    public SatPaqPropertyCompass(byte[] data) {
        super(data);
    }


    @Override
    public byte[] setCommandData() {
        byte[] cmdData = new byte[15];
        ByteBuffer buffer = ByteBuffer.wrap(cmdData);

        buffer = buffer.order(ByteOrder.LITTLE_ENDIAN);
        buffer.put(m_commandType);
        buffer.put(m_control);
        buffer.putShort(m_distance);
        buffer.putShort(m_tar);
        buffer.put(m_par);
        buffer.put(m_tae);
        buffer.put(m_reserved1);
        buffer.put(m_tme);
        buffer.put(m_reserved2);
        buffer.putShort(m_orientingRate);
        buffer.putShort(m_orientedRate);

        return buffer.array();
    }

    @Override
    public byte[] getCommandData() {
        return null;
    }

    public static byte[] stopCommandData() {
        byte[] cmdData = new byte[1];
        cmdData[0] = HGNordicCommandType.CMD_COMPASS_STOP.getValue();
        return cmdData;
    }

    public byte getControl() {
        return m_control;
    }

    public void setControl(byte control) {
        this.m_control = control;
    }

    public short getDistance() {
        return m_distance;
    }

    public void setDistance(short distance) {
        this.m_distance = distance;
    }

    public short getTar() {
        return m_tar;
    }

    public void setTar(short tar) {
        this.m_tar = tar;
    }

    public byte getPar() {
        return m_par;
    }

    public void setPar(byte par) {
        this.m_par = par;
    }

    public byte getTae() {
        return m_tae;
    }

    public void setTae(byte tae) {
        this.m_tae = tae;
    }

    public byte getReserved1() {
        return m_reserved1;
    }

    public void setReserved1(byte reserved1) {
        this.m_reserved1 = reserved1;
    }

    public byte getTme() {
        return m_tme;
    }

    public void setTme(byte tme) {
        this.m_tme = tme;
    }

    public byte getReserved2() {
        return m_reserved2;
    }

    public void setReserved2(byte reserved2) {
        this.m_reserved2 = reserved2;
    }

    public short getOrientingRate() {
        return m_orientingRate;
    }

    public void setOrientingRate(short orientingRate) {
        this.m_orientingRate = orientingRate;
    }

    public short getOrientedRate() {
        return m_orientedRate;
    }

    public void setOrientedRate(byte orientedRate) {
        this.m_orientedRate = orientedRate;
    }

    public String getParamString() {
        return String.format(Locale.US, "control=0x%02X distance=%d tar=%d par=%d tae=%d tme=%d orientingRate=%d orientedRate=%d",
                m_control, m_distance, m_tar, m_par, m_tae, m_tme, m_orientingRate, m_orientedRate);
    }
}
