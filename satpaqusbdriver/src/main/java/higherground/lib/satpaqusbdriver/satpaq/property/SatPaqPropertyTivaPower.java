package higherground.lib.satpaqusbdriver.satpaq.property;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import higherground.lib.satpaqusbdriver.satpaq.enums.HGNordicCommandType;

public class SatPaqPropertyTivaPower extends AbstractSatPaqProperty {
    private final static int TIVA_POWER_OFF = 0x00;
    private final static int TIVA_POWER_ON = 0x01;

    public SatPaqPropertyTivaPower() {
        super(HGNordicCommandType.CMD_TIVA_POWER_GET.getValue());
    }

    private byte m_tivaPower;

    public SatPaqPropertyTivaPower(byte[] data) {
        super(data);

        ByteBuffer buffer = ByteBuffer.wrap(m_propertyData);
        buffer = buffer.order(ByteOrder.LITTLE_ENDIAN);

        m_tivaPower = buffer.get();
    }

    public SatPaqPropertyTivaPower(boolean tivaPower) {
        super(HGNordicCommandType.CMD_TIVA_POWER_SET.getValue());

        if(tivaPower) {
            m_tivaPower = TIVA_POWER_ON;
        } else {
            m_tivaPower = TIVA_POWER_OFF;
        }
    }

    public boolean getTivaPower() {
        if(m_tivaPower == TIVA_POWER_ON) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public byte[] setCommandData() {
        byte[] setCmdData = new byte[2];
        setCmdData[0] = m_commandType;
        setCmdData[1] = m_tivaPower;
        return setCmdData;
    }

    @Override
    public byte[] getCommandData() {
        return getStandardGetCommandData();
    }
}
