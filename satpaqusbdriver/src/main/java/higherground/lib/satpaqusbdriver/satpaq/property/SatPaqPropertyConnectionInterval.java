package higherground.lib.satpaqusbdriver.satpaq.property;

import higherground.lib.satpaqusbdriver.satpaq.enums.HGNordicCommandType;

public class SatPaqPropertyConnectionInterval extends AbstractSatPaqProperty {

    public SatPaqPropertyConnectionInterval() {
        super(HGNordicCommandType.CMD_CONN_INTERVAL_GET.getValue());
    }

    public SatPaqPropertyConnectionInterval(byte[] data) {
        super(data);
    }

    @Override
    public byte[] setCommandData() {
        return new byte[0];
    }

    @Override
    public byte[] getCommandData() {
        return new byte[0];
    }
}
