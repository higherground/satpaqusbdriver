package higherground.lib.satpaqusbdriver.satpaq

import android.content.Context
import higherground.lib.logger.HGNormalLog

// TODO: Add unit tests so all the warnings suppressed below naturally go away.
@Suppress("unused")
object SatPaqFactory {
    private const val LOG_TAG = "SatPaqFactory"
    private var satPaqs: MutableMap<String, SatPaq> = mutableMapOf()

    /* An empty identifier string simply means that we have not selected a SatPaq yet */
    fun getSatPaqHelper(identifier: String, context: Context): SatPaq {
        val satPaqName: String = if (identifier.isNotEmpty()) identifier else "NONE"

        HGNormalLog.trace(LOG_TAG, "getSatPaqHelper: name=$satPaqName")

        if (!satPaqs.containsKey(satPaqName)) {
            lateinit var satPaqHelper: SatPaq

            when (satPaqName) {
                "USB" -> {
                    // USB Not supported yet
                    //assert(false)
                    satPaqHelper = SatPaqUSB(satPaqName, context);
                }
                "SIM" -> {
                    // Alex TODO Implement a SatPaq simulator instead of special casing
                    // SatPaq simulation in our code.
                    // Might be useful for unit testing too.
                    assert(false);
                }
                "NONE" -> {
                    // We used to support empty satpaq because callbacks were registered with it.
                    assert(false);
                }
                else -> {
                    //satPaqHelper = SatPaqBLE(satPaqName, context)
                }
            }

            satPaqs[satPaqName] = satPaqHelper
        }

        return satPaqs[satPaqName]!!
    }

    fun deleteSatPaqHelper(identifier: String) {
        satPaqs.remove(identifier)
    }
}
