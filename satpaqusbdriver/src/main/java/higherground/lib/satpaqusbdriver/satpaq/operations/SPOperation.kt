package higherground.lib.satpaqusbdriver.satpaq.operations

import higherground.lib.satpaqusbdriver.utils.HasRawValue

enum class SPResult {
    IN_PROGRESS,
    SUCCESS,
    FAILURE,
    CANCELLED,
    CONNECTED,
    DISCONNECTED,
    NOT_SUPPORTED,
}

/**
 * Bit masks for possible SatPaq operations.
 * TODO: Is there a better Kotlin way to define bit masks?
 */
enum class SPOpType(override val rawValue: Long) : HasRawValue {
    UNDEFINED(0x00000000),              // Not set yet
    CONNECT_STATUS(0x00000001),         // SatPaq Connection Status Updates
    TIVA_INFO(0x00000002),              // SatPaq Serial#, FW versions, ID, etc.
    BATTERY(0x00000004),                // Battery Updates
    TEMPERATURE(0x00000008),            // Temperature Updates
    TIVA_SEND(0x00000010),              // TIVA Command
    TIVA_RECEIVE(0x00000020),           // TIVA Command Response
    NORDIC_SEND(0x00000040),            // Nordic Command
    NORDIC_RECEIVE(0x00000080),         // Nordic Command Response
    SATELLITE_SEND(0x00000100),         // Satellite Send Message
    SATELLITE_RECEIVE(0x00000200),      // Satellite Receive Message
    SATELLITE_BROADCAST(0x00000400),    // Satellite Receive Broadcast (MCS)
    SATELLITE_STATUS(0x00000800),       // Satellite Transaction (PT Status) Update
    TIVA_JXLOG(0x00001000),             // TIVA JXLog
    TIVA_APP(0x00002000),               // TIVA App response
    NORDIC_INFO(0x00004000),            // Nordic information
    TIVA_UECHO(0x00080000),             // TIVA UEcho response
    NOT_SUPPORTED(0x80000000),          // Operation not supported by current class

    /** Convenience value for registering for all callbacks. */
    ALL(Long.MAX_VALUE),
}

open class SPOperation(
        val clientID: String = "",
        // Alex TODO TivaInfo structure could not figure out how to initialize opType in the constructor
        var opType: SPOpType = SPOpType.ALL,
        val callback: ((SPOperation) -> Unit)? = null,
        var result: SPResult = SPResult.IN_PROGRESS
) {
    fun isEqual(type: SPOpType): Boolean {
        return (opType == type)
    }
}
