package higherground.lib.satpaqusbdriver.satpaq.property;

import java.nio.ByteBuffer;

import higherground.lib.satpaqusbdriver.satpaq.enums.HGNordicCommandType;

public class SatPaqPropertyDFU extends AbstractSatPaqProperty {
    public SatPaqPropertyDFU() {
        super(HGNordicCommandType.CMD_BLE_CONTROL_CMD.getValue());
    }

    @Override
    public byte[] setCommandData() {
        byte[] cmdData = new byte[5];
        ByteBuffer buffer = ByteBuffer.wrap(cmdData);

//        buffer = buffer.order(ByteOrder.LITTLE_ENDIAN);
        buffer.put(HGNordicCommandType.CMD_BLE_CONTROL_CMD.getValue());
        buffer.put((byte)0x02);
        buffer.put((byte)0x11);
        buffer.put((byte)0x57);
        buffer.put((byte)0x19);

        return buffer.array();
    }

    @Override
    public byte[] getCommandData() {
        return getStandardGetCommandData();
    }
}
