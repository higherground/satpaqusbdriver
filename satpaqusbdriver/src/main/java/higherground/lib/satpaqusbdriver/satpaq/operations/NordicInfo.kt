package higherground.lib.satpaqusbdriver.satpaq.operations

import higherground.lib.satpaqusbdriver.satpaq.enums.StatusBits
import higherground.lib.satpaqusbdriver.utils.HGBits

/**
 * NordicLastSet is used to remember the last field that was updated.
 * This may be useful for U.I. components that are waiting on a specific update
 */

enum class NordicLastSet {
    NAME,
    SERIAL_NUMBER,
    ADVERTISED_NAME,
    MODEL_NUMBER,
    HARDWARE_REVISION,
    BOOT_LOADER_REVISION,
    FIRMWARE_REVISION,
    MANUFACTURE_NAME,
    BOARD_REVISION,
    SYSTEM_ID,
    BATTERY_LEVEL,
    TEMPERATURE,
    STATUS,
    JERK,
    NONE
}

/**
 * NordicInfo
 * The NordicInfo SPOperation includes all of the readable attributes for all of the
 * GATT Services implemented by the SatPaq's Nordic processor
 *    1. Device Information service
 *    2. Battery Service
 *    3. Custom Satpaq Service
 *
 * It is not very granular, but saves implementing a ton of special SP Operation variants.
 * The property updated is remembered as this may be useful information for U.I. components.
 */
class NordicInfo : SPOperation(opType = SPOpType.NORDIC_INFO) {
    var name = ""
        set(value) {
            field = value
            lastSet = NordicLastSet.NAME
        }

    var serialNumber = ""
        set(value) {
            field = value
            lastSet = NordicLastSet.SERIAL_NUMBER
        }

    var advertisedName = ""        // From Device Info Service (normally serialNumber)
        set(value) {
            field = value
            lastSet = NordicLastSet.ADVERTISED_NAME
        }

    var modelNumber = ""
        set(value) {
            field = value
            lastSet = NordicLastSet.MODEL_NUMBER
        }

    var hardwareRevision = ""
        set(value) {
            field = value
            lastSet = NordicLastSet.HARDWARE_REVISION
        }

    var bootloaderRevision = ""
        set(value) {
            field = value
            lastSet = NordicLastSet.BOOT_LOADER_REVISION
        }

    var firmwareRevision = ""
        set(value) {
            field = value
            lastSet = NordicLastSet.FIRMWARE_REVISION
        }

    var manufactureName = ""
        set(value) {
            field = value
            lastSet = NordicLastSet.MANUFACTURE_NAME
        }

    var boardRevision = ""
        set(value) {
            field = value
            lastSet = NordicLastSet.BOARD_REVISION
        }


    @ExperimentalUnsignedTypes
    var systemID: ULong = 0u
        set(value) {
            field = value
            lastSet = NordicLastSet.SYSTEM_ID
        }

    var batteryLevel: Int = 0
        set(value) {
            field = value
            lastSet = NordicLastSet.BATTERY_LEVEL
        }

    var temperature: Double = 0.0
        set(value) {
            field = value
            lastSet = NordicLastSet.TEMPERATURE
        }

    var status: Int = 0
        set(value) {
            field = value
            lastSet = NordicLastSet.STATUS
        }

    var jerk: Int = 0
        set(value) {
            field = value
            lastSet = NordicLastSet.JERK
        }

    @Suppress("MemberVisibilityCanBePrivate")
    var lastSet: NordicLastSet = NordicLastSet.NONE
        private set

    fun getPercentCharged() : String {
        val charge = batteryLevel.toInt()
        return "$charge%"
    }

    fun getDegreesFahrenheit() : String {
        val fahrenheit = ((temperature * 9/5) + 32).toInt()
        return "$fahrenheit\u2109"
    }

    fun getVersionInfo() : String {
        // Alex TODO: We need a nice semversion iosolator (strip thing after the major.minor.point)
        return "$bootloaderRevision/$firmwareRevision"
    }

    fun statusUpdate(newStatus: Int): Boolean {
        var updated = false

        if (status != newStatus) {
            status = newStatus
            updated = true
        }

        return updated
    }

    fun isCharging(): Boolean {
        return HGBits.isSet(status, StatusBits.Charging)
    }

    fun isTivaAwake(): Boolean {
        return HGBits.isSet(status, StatusBits.TivaAwake)
    }

    fun isTooHot(): Boolean {
        return HGBits.isSet(status, StatusBits.TooHot)
    }

    companion object {
        @JvmStatic
        fun factory(): NordicInfo {
            return NordicInfo()
        }
    }
}
