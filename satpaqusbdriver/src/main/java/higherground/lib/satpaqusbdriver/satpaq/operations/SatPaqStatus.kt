package higherground.lib.satpaqusbdriver.satpaq.operations
import com.google.gson.GsonBuilder
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

import higherground.lib.satpaqusbdriver.satpaq.enums.HGReturnEsN0Info
import higherground.lib.satpaqusbdriver.satpaq.enums.SatPaqDataType
import higherground.lib.satpaqusbdriver.utils.HGBits
import higherground.lib.satpaqusbdriver.utils.HGByteBuffer
import java.nio.ByteOrder

/**
 * Parses a SatPaqManager Status PDU
 */
class SatPaqStatus : SPOperation {
    @Expose
    @SerializedName("Flags")
    var flagString: String? = null
    @Expose
    @SerializedName("PDUs")
    var pduCount = 0
    @Expose
    @SerializedName("ACKs")
    var ackCount = 0
    @Expose
    @SerializedName("Sent")
    var pduSend = 0
    @Expose
    @SerializedName("Resent")
    var pduResend = 0
    @Expose
    @SerializedName("FEC")
    var fwdBitErrs = 0
    @Expose
    @SerializedName("EsN0")
    var esN0 = 0.0
    @Expose
    @SerializedName("RxAge")
    var rxAge = 0
    @Expose
    @SerializedName("CN0")
    var pilotCN0 = 0.0
    @Expose
    @SerializedName("PilotAge")
    var pilotAge = 0
    @Expose
    @SerializedName("FreqOffset")
    var freqOffset = 0
    @Expose
    @SerializedName("RxComplete")
    var rxComplete = 0
//    get() {
 //       return field
   // }

    @Expose
    @SerializedName("Filter")
    var filter = 0
    @Expose
    @SerializedName("State")
    var state = 0
    @Expose
    @SerializedName("ReturnEsN0")
    var returnEsN0Info: HGReturnEsN0Info? = null

    private var averageEsN0 = 0.0
    private var averageCN0 = 0.0
    private var type = 0
    private var flags: Byte = 0
    private var lastUpdate = 0.0

    constructor(pdu: ByteArray) : super(opType = SPOpType.SATELLITE_STATUS) {
        update(pdu)
    }

    constructor() : super(opType = SPOpType.SATELLITE_STATUS)

    // DD TODO - Do we really need this constructor?
    constructor(isFailing: Boolean, rxEsN0: Double, returnEsN0: HGReturnEsN0Info?) :
            super(opType = SPOpType.SATELLITE_STATUS) {
        type = PA_STATUS_TYPE
        if (isFailing) {
            flags = DECODEFAIL
        }
        esN0 = rxEsN0
        returnEsN0Info = returnEsN0
        flagString = ""
    }

    fun update(pdu: ByteArray) :Boolean {
        if (pdu.size != NORMAL_STATUS_LEN && pdu.size != RETURN_ESN0_STATUS_LEN || pdu[0] != SatPaqDataType.STATUS_RESPONSE) {
            return false
        }
        val bb = HGByteBuffer(pdu)
        bb.order(ByteOrder.BIG_ENDIAN)
        type = bb.nextU8()
        flags = bb.next8()
        pduCount = bb.nextU8()
        ackCount = bb.nextU8()
        pduSend = bb.nextU8()
        pduResend = bb.nextU8()
        fwdBitErrs = bb.nextU8()
        esN0 = bb.parseN0FromByte()
        rxAge = bb.nextU8()
        pilotCN0 = bb.parseN0FromByte()
        pilotAge = bb.nextU8()
        freqOffset = bb.next16()
        rxComplete = bb.nextU8()
        filter = bb.next16()
        state = bb.nextU8()
        returnEsN0Info = if (pdu.size == RETURN_ESN0_STATUS_LEN) {
            val returnEsN0Byte = bb.next16()
            HGReturnEsN0Info.fromValue(returnEsN0Byte and RETURN_ESN0_INFO_BIT.toInt())
        } else {
            HGReturnEsN0Info.None
        }
        lastUpdate = System.nanoTime().toDouble()
        averageCN0 = deltaAverage(averageCN0, pilotCN0)
        averageEsN0 = deltaAverage(averageEsN0, esN0)
        flagString = flagsAsString

        return true
    }

    val isFailing: Boolean
        get() = isSet(DECODEFAIL)

    fun rxDemodFailed(): Boolean {
        return isSet(LDPC_DECODE_FAIL)
    }

    private val flagsAsString: String
        get() {
            var fBits = "..."
            fBits += if (flagCompassInhibit()) "C" else "c"
            fBits += if (flagLdpcDecodeFail()) "E" else "e"
            fBits += if (flagPilot()) "P" else "p"
            fBits += if (flagAckPending()) "A" else "a"
            fBits += if (flagProximity()) "X" else "x"
            fBits += if (flagTransmistting()) "T" else "t"
            fBits += if (flagHalfRate()) "H" else "h"
            fBits += if (flagRxRecent()) "R" else "r"
            return fBits
        }

    private fun isSet(mask: Byte) : Boolean {
        return HGBits.isSet(flags, mask)
    }

    fun flagCompassInhibit(): Boolean {
        return isSet(COMPASS_INHIBIT)
    }

    fun flagLdpcDecodeFail(): Boolean {
        return isSet(LDPC_DECODE_FAIL)
    }

    fun flagPilot(): Boolean {
        return isSet(PILOT)
    }

    fun flagAckPending(): Boolean {
        return isSet(ACK_PENDING)
    }

    fun flagProximity(): Boolean {
        return isSet(PROXIMITY)
    }

    fun flagTransmistting(): Boolean {
        return isSet(TRANSMITTING)
    }

    fun flagHalfRate(): Boolean {
        return isSet(HALF_RATE)
    }

    fun flagRxRecent(): Boolean {
        return isSet(RX_RECENT)
    }

    val dataString: String
        get() {
            var retString = "\r\n"
            retString += String.format("PTStatus_Flag:%s \r\n", flagsAsString)
            retString += String.format("PTStatus_PDUs:%s \r\n", pduCount)
            retString += String.format("PTStatus_ACKs:%s \r\n", ackCount)
            retString += String.format("PTStatus_Sent:%s \r\n", pduSend)
            retString += String.format("PTStatus_Resent:%s \r\n", pduResend)
            retString += String.format("PTStatus_RxComplete:%s \r\n", rxComplete)
            retString += String.format("PTStatus_BER:%s \r\n", fwdBitErrs)
            retString += String.format("PTStatus_EsN0:%s \r\n", esN0)
            retString += String.format("PTStatus_RxAge:%s \r\n", rxAge)
            retString += String.format("PTStatus_CN0:%s \r\n", pilotCN0)
            retString += String.format("PTStatus_PilotAge:%s \r\n", pilotAge)
            retString += String.format("PTStatus_FreqOffset:%s \r\n", freqOffset)
            retString += String.format("PTStatus_State:%s \r\n", state)
            retString += String.format("PTStatus_RtnEsN0:%s \r\n", returnEsN0InfoString)
            return retString
        }

    val jsonString: String
        get() {
            val gson = GsonBuilder()
                    .excludeFieldsWithoutExposeAnnotation()
                    .create()
            return gson.toJson(this)
        }

    private fun deltaAverage(curAverage: Double, newVal: Double): Double {
        var curAverage = curAverage
        if (curAverage == 0.0) {
            curAverage = newVal
        } else if (Math.abs(curAverage - newVal) >= 7.0) {
            curAverage = newVal
        } else if (curAverage + 0.1 < newVal) {
            curAverage += 0.1
        } else if (curAverage - 0.1 > newVal) {
            curAverage -= 0.1
        }
        return curAverage
    }

    private val returnEsN0InfoString: String
        private get() = when (returnEsN0Info) {
            HGReturnEsN0Info.Inactive -> "Inactive"
            HGReturnEsN0Info.Waiting -> "Waiting"
            HGReturnEsN0Info.Timeout -> "Timeout"
            HGReturnEsN0Info.Unused -> "Unused"
            HGReturnEsN0Info.NoUpdates -> "No updates"
            HGReturnEsN0Info.Poor -> "Poor Signal"
            HGReturnEsN0Info.Okay -> "Okay Signal"
            HGReturnEsN0Info.Good -> "Good Signal"
            HGReturnEsN0Info.None -> "None"
            else -> "Unknown"
        }

    companion object {
        private const val NORMAL_STATUS_LEN = 16
        private const val RETURN_ESN0_STATUS_LEN = 17
        private const val PA_STATUS_TYPE = 0x51
        // Bit Mask Definitions
        private const val LDPC_DECODE_FAIL: Byte = 64
        private const val COMPASS_INHIBIT = 0x80.toByte()
        private const val DECODEFAIL: Byte = 0x40
        private const val PILOT: Byte = 0x20
        private const val ACK_PENDING: Byte = 0x10
        private const val PROXIMITY: Byte = 0x08
        private const val TRANSMITTING: Byte = 0x04
        private const val HALF_RATE: Byte = 0x02
        private const val RX_RECENT: Byte = 0x01
        private const val RETURN_ESN0_INFO_BIT: Byte = 0x07
    }
}
