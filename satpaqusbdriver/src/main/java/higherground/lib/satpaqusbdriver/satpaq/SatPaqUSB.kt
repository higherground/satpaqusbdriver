package higherground.lib.satpaqusbdriver.satpaq

import android.content.Context
import higherground.lib.logger.HGNormalLog
import higherground.lib.satpaq.NordicServices
import higherground.lib.satpaqusbdriver.*
import higherground.lib.satpaqusbdriver.JSONDefragger
import higherground.lib.satpaqusbdriver.satpaq.enums.HGNordicCommandType
//import higherground.lib.satpaqusbdriver.SatPaqUsbDriver
//import higherground.lib.satpaqusbdriver.UsbConnectStatus
//import higherground.lib.satpaqusbdriver.UsbDriverObserved
//import higherground.lib.satpaqusbdriver.UsbMessage
import higherground.lib.satpaqusbdriver.satpaq.operations.*
import higherground.lib.satpaqusbdriver.satpaq.property.*
import higherground.lib.satpaqusbdriver.satpaq.enums.GattCharacteristic
import higherground.lib.satpaqusbdriver.satpaq.enums.GattOperation
import higherground.lib.satpaqusbdriver.satpaq.enums.GattService
import java.util.*


class SatPaqUSB(satPaqName: String, context: Context) : SatPaq(satPaqName, context) {

    companion object {
        const val LOG_TAG = "SatPaqUSB(C)"
    }

    val usbDriver: SatPaqUsbDriver
    val defragger: JSONDefragger

    //private lateinit var usbDriver: SatPaqUsbDriver

    init {
        //usbDriver = SatPaqUsbDriver(context)
        //usbDriver.usbRegisterObservers(LOG_TAG, usbDriverCallback())
        usbDriver = SatPaqUsbDriver.getInstance()
        usbDriver.init(context)
        usbDriver.usbRegisterObservers(LOG_TAG, usbDriverCallback())
        defragger = JSONDefragger
        defragger.registerListener(jsonDefraggerCallback())
    }

    inner class usbDriverCallback : UsbDriverObserved {
        override fun onUsbSend(msg: UsbMessage) {
            HGNormalLog.trace(LOG_TAG, "onUsbSend: " + msg.usbMessage + " // " + msg.usbRC.toString())
        }

        override fun onReceived(msg: UsbMessage) {
            HGNormalLog.trace(LOG_TAG, "onUsbReceived: " + msg.usbMessage)
        }

        override fun onConnectChange(status: UsbConnectStatus) {
            HGNormalLog.trace(LOG_TAG, "onConnectChange: $status")
            var spresult: SPResult = SPResult.NOT_SUPPORTED

            // Turn off echo upon connect verification.
            if(status == UsbConnectStatus.usbConnected) {
                val echooff = UsbMessage()
                echooff.usbMessage = "echo off\n"
                usbDriver.usbSend(echooff)
            }

            when (status) {
                UsbConnectStatus.usbConnectFailed -> spresult = SPResult.FAILURE
                UsbConnectStatus.usbConnected -> spresult = SPResult.CONNECTED
                UsbConnectStatus.usbDisconnected -> spresult = SPResult.DISCONNECTED
                UsbConnectStatus.usbConnecting -> spresult = SPResult.NOT_SUPPORTED
            }

            val spoperation = SPOperation(
                    opType = SPOpType.CONNECT_STATUS,
                    result = spresult
            )

            SatPaqCallbacks.clientCallback(spoperation)
        }
    }

    inner class jsonDefraggerCallback: JSONDefraggerListener {
        override fun onMessageSent(message: UsbMessage) {
            HGNormalLog.trace(LOG_TAG, "onMessageSent: %s", message)
        }

        override fun onMessageReceived(message: String) {
            HGNormalLog.trace(LOG_TAG, "onMessageReceived: %s", message)

            val ep = SatPaqUsbEndpoints.demarshalUSBData(message)
            if(ep == null) {
                HGNormalLog.info(LOG_TAG, "onMessageReceived: demarshal failed.")
                return
            }

            HGNormalLog.info(LOG_TAG, "onMessageReceived: Returned message type: %s", ep.type.toString())

            if(ep.data.isEmpty()) ep.data = ByteArray(0)

            if(ep.type == EndpointType.NordicGATT) {
                HGNormalLog.info(LOG_TAG, "onMessageReceived: Message type: NordicGatt")

                // Kotlin can't handle this state
                if(ep.data == null) ep.data = ByteArray(0);
                val spoperation = NordicServices.parse(ep.service, ep.characteristic, ep.data)

                SatPaqCallbacks.clientCallback(spoperation)
            } else if(ep.type == EndpointType.TivaCommand) {
                // TODO: Make sure it works with this when implemented.
                HGNormalLog.info(LOG_TAG, "onMessageReceived: Message type: TivaCommand")
                val spoperation = TivaServices.parse(ep.type, ep.usbMessage)

                SatPaqCallbacks.clientCallback(spoperation)
            } else {
                HGNormalLog.error(LOG_TAG, "onMessageReceived: Unknown message type: %s", ep.type.toString())
            }
        }
    }

    override fun connectSatPaq() {
        // TODO Trifecta should this go through JSON Defragger or directly to the SatPaqUSB?
        usbDriver.usbConnect()
    }

    override fun disconnectSatPaq() {
        // TODO Trifecta should this go through JSON Defragger or directly to the SatPaqUSB?
        usbDriver.usbDisconnect()
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun cancelConnect() {
        TODO("Not yet implemented")
    }

    /**
     * Chicken and egg...
     * SatPaq USB cannot operate until this member is executed via SatPaqBLE class
     * prod set -sio -force
     */
    override fun enableUSB() {
        val opResult = SPOperation(
                clientID = LOG_TAG, // Must be unique
                opType = SPOpType.NOT_SUPPORTED,
                result = SPResult.NOT_SUPPORTED
        )

        SatPaqCallbacks.clientCallback(opResult)
    }

    override fun sendTivaCommand(command: String) {
        // Call marshal USBEndPoints.marshallTIVACommand(string)
        // Then send the Marshalled string to usbDriver.usbSend()
        val usbMessage = SatPaqUsbEndpoints.marshalTIVACommand(command)
        usbDriver.usbSend(usbMessage)
    }


    /********
     * GATT Service Battery Service Functions
     *******/

    override fun getBatteryLevel() {
        HGNormalLog.trace(LOG_TAG, "getSatPaqBattery")
        sendNordicCommand(
                GattService.Battery,
                GattCharacteristic.BatteryLevel,
                GattOperation.ReadValue,
                null
        )
    }

    /********
     * GATT Service Device Information
     *******/

    override fun getNordicHardwareRevision() {
        HGNormalLog.trace(LOG_TAG, "getNordicHardwareRevision")
        sendNordicCommand(
                GattService.DeviceInfo,
                GattCharacteristic.HardwareRevision,
                GattOperation.ReadValue,
                null
        )
    }

    override fun getNordicBootloaderRevision() {
        HGNormalLog.trace(LOG_TAG, "getNordicBootloaderRevision")
        sendNordicCommand(
                GattService.DeviceInfo,
                GattCharacteristic.SoftwareRevision,
                GattOperation.ReadValue,
                null
        )
    }

    override fun getNordicFirmwareRevision() {
        HGNormalLog.trace(LOG_TAG, "getNordicBootloaderRevision")
        sendNordicCommand(
                GattService.DeviceInfo,
                GattCharacteristic.FirmwareRevision,
                GattOperation.ReadValue,
                null
        )
    }

    override fun getNordicManufactureName() {
        HGNormalLog.trace(LOG_TAG, "getNordicManufactureName")
        sendNordicCommand(
                GattService.DeviceInfo,
                GattCharacteristic.ManufactureName,
                GattOperation.ReadValue,
                null
        )
    }

    override fun getNordicBoardRevision() {
        HGNormalLog.trace(LOG_TAG, "getNordicBoardRevision")
        sendNordicCommand(
                GattService.DeviceInfo,
                GattCharacteristic.BoardRevision,
                GattOperation.ReadValue,
                null
        )
    }

    override fun getNordicSystemID() {
        HGNormalLog.trace(LOG_TAG, "getNordicSystemID")
        sendNordicCommand(
                GattService.DeviceInfo,
                GattCharacteristic.SystemID,
                GattOperation.ReadValue,
                null
        )
    }

    override fun getNordicModelNumber() {
        HGNormalLog.trace(LOG_TAG, "getNordicModelNumber")
        sendNordicCommand(
                GattService.DeviceInfo,
                GattCharacteristic.ModelNumber,
                GattOperation.ReadValue,
                null
        )

    }

    /********
     * GATT Service SatPaq: Characteristic Status
     *******/

    override fun getSatPaqStatus() {
        // TODO DO we need a reader? We should get updates from time to time
    }

    /********
     * GATT Service SatPaq: Characteristic Command Functions
     *******/
    override fun setSerialNumber(serialNumber: String?) {
        HGNormalLog.trace(LOG_TAG, "setSerialNumber: %s", serialNumber)
        val sn = SatPaqPropertySN(serialNumber)
        spServiceCommandWrite(sn.setCommandData())
    }

    override fun getSerialNumber() {
        HGNormalLog.trace(LOG_TAG, "getSerialNumber")
        val sn = SatPaqPropertySN()
        spServiceCommandRead(sn.getCommandData())
    }

    override fun setSatPaqName(satPaqName: String?) {
        HGNormalLog.trace(LOG_TAG, "setSatPaqName")
        val name = SatPaqPropertyName(satPaqName)
        spServiceCommandWrite(name.setCommandData())
    }

    override fun getSatPaqName() {
        HGNormalLog.trace(LOG_TAG, "getSatPaqName")
        val name = SatPaqPropertyName()
        spServiceCommandRead(name.getCommandData())
    }

    override fun getTivaPower() {
        HGNormalLog.trace(LOG_TAG, "getTivaPower")
        val tivaPower = SatPaqPropertyTivaPower()
        spServiceCommandRead(tivaPower.commandData)
    }

    override fun startTiva() {
        HGNormalLog.trace(LOG_TAG, "startTiva")
        val tivaPower = SatPaqPropertyTivaPower(true)

        spServiceCommandWrite(tivaPower.setCommandData())
    }

    override fun stopTiva() {
        HGNormalLog.info(LOG_TAG, "Stopping Tiva")
        val tivaPower = SatPaqPropertyTivaPower(false)

        spServiceCommandWrite(tivaPower.setCommandData())
    }

    override fun setConfigParams(params: ByteArray) {
        HGNormalLog.trace(LOG_TAG, "setConfigParams")
        val config = SatPaqPropertyConfig(params)

        spServiceCommandWrite(config.setCommandData())
    }

    override fun getConfigParams() {
        HGNormalLog.trace(LOG_TAG, "getConfigParams")
        val config = SatPaqPropertyConfig()

        spServiceCommandRead(config.getCommandData())
    }

    override fun setConnectionInterval(params: ByteArray) {
        HGNormalLog.trace(LOG_TAG, "setConnectionInterval")
        val connectionInterval = SatPaqPropertyConnectionInterval(params)

        spServiceCommandWrite(connectionInterval.setCommandData())
    }

    override fun getConnectionInterval() {
        HGNormalLog.trace(LOG_TAG, "getConnectionInterval")
        val connectionInterval = SatPaqPropertyConnectionInterval()

        spServiceCommandRead(connectionInterval.getCommandData())
    }

    override fun getSatPaqTemperature() {
        HGNormalLog.trace(LOG_TAG, "getSatPaqTemperature")
        val temperature = SatPaqPropertyTemperature()

        spServiceCommandRead(temperature.getCommandData())
    }

    override fun getAccelVector() {
        HGNormalLog.trace(LOG_TAG, "getAccelVector")
        val accel = SatPaqPropertyAccel()

        spServiceCommandRead(accel.getCommandData())
    }

    override fun enableDFU() {
        HGNormalLog.trace(LOG_TAG, "enableDFU")
        val dfu = SatPaqPropertyDFU()

        spServiceCommandWrite(dfu.setCommandData())
    }

    override fun executeLogCommand(logData: ByteArray) {
        HGNormalLog.trace(LOG_TAG, "executeLogCommand")
        val logData = SatPaqPropertyLogData(logData)

        spServiceCommandWrite(logData.setCommandData())
    }

    override fun setDelayTimer(delay: Int, timerIndex: Int) {
        HGNormalLog.info(LOG_TAG, "setting satpaq delay timer, interval is %d, index is %d", delay, timerIndex)
        val delayTimerCommand = SatPaqPropertyDelayTimer()

        // Index of timer to start or stop
        delayTimerCommand.setTimer_index(timerIndex.toByte())
        /**
         * Control Bits (in the request):
         * Bit 0: 0 = One shot    : 1 = Continuous
         */
        delayTimerCommand.setControl(1.toByte())
        delayTimerCommand.setDelay(delay)

        spServiceCommandWrite(delayTimerCommand.setCommandData())
    }

    override fun startJerk(sampleInterval: Int, averageLen: Int) {
        HGNormalLog.trace(LOG_TAG, "startJerk")
        val jerkStartCmd = SatPaqPropertyJerk(HGNordicCommandType.CMD_JERK_START.value)
        jerkStartCmd.setSampleInterval(sampleInterval)
        jerkStartCmd.setAverageLen(averageLen.toByte())
        spServiceCommandWrite(jerkStartCmd.setCommandData())
    }

    override fun stopJerk() {
        HGNormalLog.trace(LOG_TAG, "stopJerk")
        val jerkStopCmd = SatPaqPropertyJerk(HGNordicCommandType.CMD_JERK_STOP.value)
        spServiceCommandWrite(jerkStopCmd.setCommandData())
    }

    override fun getJerk() {
        HGNormalLog.trace(LOG_TAG, "getJerk")
        val jerkCmd = SatPaqPropertyJerk(HGNordicCommandType.CMD_JERK_GET.value)
        spServiceCommandRead(jerkCmd.getCommandData())
    }

    fun spServiceCommandRead(command: ByteArray) {
        sendNordicCommand(GattService.SatPaq,
                GattCharacteristic.Command,
                GattOperation.ReadValue,
                command)
    }

    fun spServiceCommandWrite(command: ByteArray) {
        sendNordicCommand(
                GattService.SatPaq,
                GattCharacteristic.Command,
                GattOperation.WriteValue,
                command
        )
    }

    fun sendNordicCommand(
            service: GattService,
            characteristic: GattCharacteristic,
            operation: GattOperation,
            command: ByteArray?) {
        // Call USBEndPoints.marshalNordicCommand(service, characteristic, operation, command)
        // Then send the marsharlled string to usbDriver.usbSend()
        val usbMessage = SatPaqUsbEndpoints.marshalNordicCommand(service, characteristic, operation, command)
        usbDriver.usbSend(usbMessage)
    }

    override fun sendMessageData(message: ByteArray) {
        TODO("Not yet implemented")
    }

    override fun sendFWUpdate(data: ByteArray?) {
        TODO("Not yet implemented")
    }

    override fun stopTransmit() {
        TODO("Not yet implemented")
    }

    override fun cancelTransmit() {
        TODO("Not yet implemented")
    }

    override fun isNordicRecoveryNeeded(): Boolean {
        TODO("Not yet implemented")
    }

}
