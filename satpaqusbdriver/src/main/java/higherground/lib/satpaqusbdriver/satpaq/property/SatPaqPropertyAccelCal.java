package higherground.lib.satpaqusbdriver.satpaq.property;

import higherground.lib.satpaqusbdriver.satpaq.enums.HGNordicCommandType;

public class SatPaqPropertyAccelCal extends AbstractSatPaqProperty {
    public SatPaqPropertyAccelCal() {
        super(HGNordicCommandType.CMD_ACCEL_CAL_GET.getValue());
    }

    public SatPaqPropertyAccelCal(byte[] data) {
        super(data);
    }

    @Override
    public byte[] setCommandData() {
        return new byte[0];
    }

    @Override
    public byte[] getCommandData() {
        return new byte[0];
    }
}
