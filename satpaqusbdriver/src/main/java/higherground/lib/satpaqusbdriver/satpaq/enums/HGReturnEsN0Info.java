package higherground.lib.satpaqusbdriver.satpaq.enums;

public enum HGReturnEsN0Info {
    Inactive(0),
    Waiting(1),
    Timeout(2),
    Unused(3),
    NoUpdates(4),
    Poor(5),
    Okay(6),
    Good(7),
    None(8);

    private int mValue;

    HGReturnEsN0Info(int value) {
        mValue = value;
    }

    public int getValue() {
        return mValue;
    }

    public static HGReturnEsN0Info fromValue(int value) {
        for (HGReturnEsN0Info info:HGReturnEsN0Info.values()) {
            if(info.getValue() == value) {
                return info;
            }
        }

        return HGReturnEsN0Info.None;
    }
}
