package higherground.lib.satpaqusbdriver.satpaq.property;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import higherground.lib.satpaqusbdriver.satpaq.enums.HGNordicCommandType;

public class SatPaqPropertyJerk extends AbstractSatPaqProperty {
    private final static int SATPAQ_NAME_MAX = 16;

    private int sampleInterval;
    private byte averageLen;

    private int jerkMagnitude;


    public SatPaqPropertyJerk(byte commandType) {
        super(commandType);
    }

    public SatPaqPropertyJerk(byte[] data) {
        super(data);

        ByteBuffer buffer = ByteBuffer.wrap(m_propertyData);
        buffer = buffer.order(ByteOrder.LITTLE_ENDIAN);
        jerkMagnitude = buffer.getShort();
    }

    @Override
    public byte[] setCommandData() {
        if(m_commandType == HGNordicCommandType.CMD_JERK_START.getValue()) {
            byte[] cmdData = new byte[4];
            ByteBuffer buffer = ByteBuffer.wrap(cmdData);

            buffer = buffer.order(ByteOrder.LITTLE_ENDIAN);
            buffer.put(m_commandType);
            buffer.putShort((short)sampleInterval);
            buffer.put(averageLen);

            return buffer.array();
        } else {
            byte[] cmdData = new byte[1];
            ByteBuffer buffer = ByteBuffer.wrap(cmdData);

            buffer = buffer.order(ByteOrder.LITTLE_ENDIAN);
            buffer.put(m_commandType);

            return buffer.array();
        }

    }

    @Override
    public byte[] getCommandData() {
        return getStandardGetCommandData();
    }

    public int getSampleInterval() {
        return sampleInterval;
    }

    public int getJerkMagnitude() {
        return jerkMagnitude;
    }

    public byte getAverageLen() {
        return averageLen;
    }

    public void setAverageLen(byte averageLen) {
        this.averageLen = averageLen;
    }

    public void setSampleInterval(int sampleInterval) {
        this.sampleInterval = sampleInterval;
    }

    public void setJerkMagnitude(int jerkMagnitude) {
        this.jerkMagnitude = jerkMagnitude;
    }
}
