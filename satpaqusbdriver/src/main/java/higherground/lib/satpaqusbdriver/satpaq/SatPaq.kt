package higherground.lib.satpaqusbdriver.satpaq

import android.content.Context
import higherground.lib.satpaq.NordicServices
import higherground.lib.satpaqusbdriver.satpaq.enums.*
//import higherground.lib.satpaqusbdriver.callbacks.HGSatPaqDFUEnabledCallback
//import higherground.lib.satpaq.callbacks.HGTivaCommandCallback
//import higherground.lib.satpaq.callbacks.HGTivaStoppedCallback
import higherground.lib.satpaqusbdriver.satpaq.operations.TivaInfo

/**
 * Gatt Service and Characteristic definitions.
 * Used internally and for ncmd address by SatPaq USB
 * NOTE: Keep this in sync with the SatPaq Firmware definitions
 * See: SatPaq Monitor User's Guide
 * https://docs.google.com/document/d/1GvSSDdd1OoxwkZg9tkI9D847lBvt1mQnZQPD1ECLHAk/edit#heading=h.ycvas6yq4tf0
 */

abstract class SatPaq(val satPaqName: String, val context: Context) {

    // TODO - Hack to easily get the battery level
    var satpaqBattery = 0

    protected var satPaqConnectState = SatPaqConnectionState.DISCONNECTED

    open fun isConnected() : Boolean {
        return (satPaqConnectState == SatPaqConnectionState.CONNECTED)
    }
    fun getConnectState() : SatPaqConnectionState {
        return satPaqConnectState
    }

    var lastTivaInfo : TivaInfo? = null

    companion object {
        private const val LOG_TAG = "SatPaq"
    }

    private var connectionState: SatPaqConnectionState = SatPaqConnectionState.DISCONNECTED

    abstract fun connectSatPaq()

    abstract fun disconnectSatPaq()

    abstract fun cancelConnect()

    abstract fun enableUSB()

    abstract fun sendTivaCommand(command: String)

    abstract fun sendMessageData(message: ByteArray)

    abstract fun sendFWUpdate(data: ByteArray?)
    abstract fun stopTransmit()
    abstract fun cancelTransmit()
    abstract fun isNordicRecoveryNeeded(): Boolean

    fun refresh() {
        refreshTiva()
        refreshNordic()
    }

    fun refreshTiva() {
        getTivaSVer()
    }

    fun getTivaSVer() {
        sendTivaCommand("sver")
    }

    fun getTivaApp() {
        sendTivaCommand("app")
    }

    fun getTivaJXlog() {
        sendTivaCommand("adc halt; status stop; jxlog")
    }

    fun setTivaBroadcastMask(mask: String) {
        sendTivaCommand("tset macbmask $mask")
    }

    // DD TODO - Possible to call before its initialized
    fun getTivaInfo() : TivaInfo? {
        return lastTivaInfo
    }

    fun refreshNordic() {
        getSatPaqName()
        getSerialNumber()
        getNordicModelNumber()
        getNordicHardwareRevision()
        getNordicBootloaderRevision()
        getNordicFirmwareRevision()
        getNordicManufactureName()
        getNordicBoardRevision()
        //getNordicSystemID()
        getBatteryLevel()
        getSatPaqTemperature()
        getSatPaqStatus()
        getJerk()
    }

    // DD TODO REview This wone
    fun getSatPaqBattery(): Int {
        return satpaqBattery
    }

    // DD TODO Review this one
    fun isSatPaqAvailable(): Boolean {
        return (satPaqConnectState == SatPaqConnectionState.CONNECTED)
    }

    fun getNordicSerialNumber(): String {
        return NordicServices.nordicInfo.serialNumber
    }

    fun isBatteryCharging(): Boolean {
        return NordicServices.nordicInfo.isCharging()
    }

    fun getTemperature(): Double {
        return NordicServices.nordicInfo.temperature
    }

    fun isTooHot(): Boolean {
        return NordicServices.nordicInfo.isTooHot()
    }

    /********
     * GATT Service Battery Service Functions
     *******/
    abstract fun getBatteryLevel()

    /********
     * GATT Service Device Information
     *******/
    abstract fun getNordicHardwareRevision()
    abstract fun getNordicBootloaderRevision()
    abstract fun getNordicFirmwareRevision()
    abstract fun getNordicManufactureName()
    abstract fun getNordicBoardRevision()
    abstract fun getNordicSystemID()
    abstract fun getNordicModelNumber()

    /********
     * GATT Service SatPaq: Characteristic Status
     *******/
    abstract fun getSatPaqStatus()

    /********
     * GATT Service SatPaq: Characteristic Command Functions
     *******/
    abstract fun setSerialNumber(serialNumber: String?)
    abstract fun getSerialNumber()
    abstract fun setSatPaqName(satPaqName: String?)
    abstract fun getSatPaqName()
    abstract fun getTivaPower()
    abstract fun startTiva()
    abstract fun stopTiva()
    abstract fun setConfigParams(params: ByteArray)
    abstract fun getConfigParams()
    abstract fun setConnectionInterval(params: ByteArray)
    abstract fun getConnectionInterval()
    abstract fun getSatPaqTemperature()
    abstract fun getAccelVector()
    abstract fun enableDFU()
    abstract fun executeLogCommand(logData: ByteArray)
    abstract fun setDelayTimer(delay: Int, timerIndex: Int)
    abstract fun startJerk(sampleInterval: Int, averageLen: Int)
    abstract fun stopJerk()
    abstract fun getJerk()

/*
    TODO Uncomment and implement as needed



    abstract fun sendMessageData(message: ByteArray)

    abstract fun stopTiva()

    abstract fun getAccelVector()

    abstract fun startJerk(sampleInterval: Int, averageLen: Int)

    abstract fun stopJerk()

    abstract fun getJerk()

    abstract fun stopTransmit()

    // DD TODO Why do we need a stopTransmit and a cancelTransmit
    abstract fun cancelTransmit()


    abstract fun isSatPaqAvailable(): Boolean

    abstract fun isNordicRecoveryNeeded(): Boolean

    // DD TODO Remove function once app starts using LiveData
    abstract fun cleanCallbacks()

    // DD TODO Remove function once app starts using LiveData
    abstract fun setStopTivaCallback(callback: HGTivaStoppedCallback)

    // DD TODO Remove function once app starts using LiveData
    fun setDFUEnabledCallback(callback: HGSatPaqDFUEnabledCallback) {
    }

    // DD TODO Remove function once app starts using LiveData
    abstract fun addTivaCommandCallback(key: HGTivaCommandType, callback: HGTivaCommandCallback?): Unit

    abstract fun removeTivaCommandCallback(key: HGTivaCommandType)


    abstract fun enableDFU()
    abstract fun sendFWUpdate(data: ByteArray?)

    abstract fun setDelayTimer(interval: Int, index: Int)
*/
}