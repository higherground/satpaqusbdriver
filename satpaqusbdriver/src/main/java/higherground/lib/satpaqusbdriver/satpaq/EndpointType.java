package higherground.lib.satpaqusbdriver.satpaq;

public enum EndpointType {
    Unknown,
    NordicGATT,
    TivaCommand,
    Satellite,
    PTStatus
}
