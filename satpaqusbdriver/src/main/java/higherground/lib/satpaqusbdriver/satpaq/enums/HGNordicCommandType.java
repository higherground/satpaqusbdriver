package higherground.lib.satpaqusbdriver.satpaq.enums;

public enum HGNordicCommandType {
    CMD_LOOP_BACK_SET          (0x00),
    CMD_SN_SET                 (0x01),
    CMD_SN_GET                 (0x02),
    CMD_TIVA_POWER_SET         (0x03),
    CMD_TIVA_POWER_GET         (0x04),
    CMD_SATPAQ_NAME_SET        (0x05),
    CMD_SATPAQ_NAME_GET        (0x06),
    CMD_CONFIG_PARAM_SET       (0x07),
    CMD_CONFIG_PARAM_GET       (0x08),
    CMD_CONN_INTERVAL_SET      (0x09),
    CMD_CONN_INTERVAL_GET      (0x0a),
    CMD_COMPASS_STATUS_GET     (0x0b),
    CMD_COMPASS_START          (0x0c),
    CMD_COMPASS_STOP           (0x0d),
    CMD_COMPASS_UPDATE         (0x0e),
    CMD_ERROR_INFO_GET         (0x0f),
    CMD_COMPASS_RAW            (0x10),
    CMD_COMPASS_DEBUG          (0x11),
    CMD_BLE_CONTROL_CMD        (0x12),
    CMD_ACCEL_CAL_RUN          (0x13),
    CMD_ACCEL_CAL_UPDATE       (0x14),
    CMD_ACCEL_CAL_GET          (0x15),
    CMD_SOFT_IRON_CAL_SET      (0x16),
    CMD_SOFT_IRON_CAL_GET      (0x17),
    CMD_COMPASS_STAND_BY       (0x18),
    CMD_LOG_START              (0x19),
    CMD_LOG_STOP               (0x1a),
    CMD_LOG_DUMP               (0x1b),
    CMD_LOG_COMMAND            (0x1c),
    CMD_LOG_DATA               (0x1d),
    CMD_PHONE_SENSOR_FAST      (0x1e),
    CMD_PHONE_SENSOR_SLOW      (0x1f),
    CMD_NORDIC_TEMP_GET        (0x20),
    CMD_ACCEL_GET              (0x21),
    CMD_DELAY_TIMER            (0x22),
    CMD_JERK_START             (0x23),
    CMD_JERK_STOP              (0x24),
    CMD_JERK_GET               (0x25),
    CMD_UNSUPPORTED            (0xff);

    private int value;

    HGNordicCommandType(int typeValue) {
        value = typeValue;
    }

    public byte getValue() {
        return (byte)value;
    }

    public static HGNordicCommandType fromInt(int value) {
        for (HGNordicCommandType type : HGNordicCommandType.values()) {
            if (type.getValue() == value) {
                return type;
            }
        }

        return HGNordicCommandType.CMD_UNSUPPORTED;
    }
}
