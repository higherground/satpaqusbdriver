package higherground.lib.satpaqusbdriver.satpaq.operations

import com.google.gson.GsonBuilder
import com.google.gson.annotations.SerializedName

class TivaApp : SPOperation {
    @SerializedName("app")
    var data: String = ""

    // Alex TODO - this does not seem to intialize the optype
    constructor(jsonStr: String) : super(opType = SPOpType.TIVA_APP) {
        factory(jsonStr)
    }

    companion object {
        @JvmStatic
        fun factory(serializedString: String?): TivaApp {
            val json = GsonBuilder().create()
            return json.fromJson(serializedString, TivaApp::class.java)
        }
    }
}

