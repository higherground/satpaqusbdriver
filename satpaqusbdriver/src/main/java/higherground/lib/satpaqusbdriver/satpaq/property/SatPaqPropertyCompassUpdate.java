package higherground.lib.satpaqusbdriver.satpaq.property;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Locale;

import higherground.lib.satpaqusbdriver.satpaq.enums.HGNordicCommandType;

public class SatPaqPropertyCompassUpdate extends AbstractSatPaqProperty {
    byte    m_flags;
    short   m_heading;                // heading angle in radians q12
    short   m_pitch;                  // pitch angle in radians q12
    byte    m_calibrationState;
    byte    m_calibrationScore;
    short   m_quarternion_w;          // rotation quaternion w q14
    short   m_quarternion_x;          // rotation quaternion x q14
    short   m_quarternion_y;          // rotation quaternion y q14
    short   m_quarternion_z;          // rotation quaternion z q14

    public SatPaqPropertyCompassUpdate() {
        super(HGNordicCommandType.CMD_COMPASS_UPDATE.getValue());
    }

    public SatPaqPropertyCompassUpdate(byte[] data) {
        super(data);

        ByteBuffer byteBuffer = ByteBuffer.wrap(m_propertyData);
        byteBuffer = byteBuffer.order(ByteOrder.LITTLE_ENDIAN);

        m_flags = byteBuffer.get();
        m_heading = byteBuffer.getShort();
        m_pitch = byteBuffer.getShort();
        m_calibrationState = byteBuffer.get();
        m_calibrationScore  = byteBuffer.get();
        m_quarternion_w = byteBuffer.getShort();
        m_quarternion_x = byteBuffer.getShort();
        m_quarternion_y = byteBuffer.getShort();
        m_quarternion_z = byteBuffer.getShort();
    }

    @Override
    public byte[] setCommandData() {
        return new byte[0];
    }

    @Override
    public byte[] getCommandData() {
        return new byte[0];
    }

    public String getLogString() {
        return String.format(Locale.US, "%d,%d,%d,%d,%d,%d,%d,%d,%d,%d",
                m_commandType,
                m_flags,
                m_heading,
                m_pitch,
                m_calibrationState,
                m_calibrationScore,
                m_quarternion_w,
                m_quarternion_x,
                m_quarternion_y,
                m_quarternion_z);
    }

    public byte getFlags() {
        return m_flags;
    }

    public short getHeading() {
        return m_heading;
    }

    public short getPitch() {
        return m_pitch;
    }

    public byte getCalibrationState() {
        return m_calibrationState;
    }

    public byte getCalibrationScore() {
        return m_calibrationScore;
    }

    public short getQuarternion_w() {
        return m_quarternion_w;
    }

    public short getQuarternion_x() {
        return m_quarternion_x;
    }

    public short getQuarternion_y() {
        return m_quarternion_y;
    }

    public short getQuarternion_z() {
        return m_quarternion_z;
    }
}
