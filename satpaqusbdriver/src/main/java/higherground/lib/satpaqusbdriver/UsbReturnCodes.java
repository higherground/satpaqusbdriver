package higherground.lib.satpaqusbdriver;

public enum UsbReturnCodes {
    OKAY, SEND_FAIL, IO_ERROR, DISCONNECTED
}