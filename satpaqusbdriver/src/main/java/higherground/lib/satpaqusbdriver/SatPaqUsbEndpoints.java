package higherground.lib.satpaqusbdriver;

import android.telephony.gsm.GsmCellLocation;
import android.text.GetChars;
import android.util.Base64;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.util.Iterator;
import java.util.Objects;

import higherground.lib.logger.HGNormalLog;
import higherground.lib.satpaqusbdriver.satpaq.EndpointType;
import higherground.lib.satpaqusbdriver.satpaq.SatPaq;
import higherground.lib.satpaqusbdriver.satpaq.enums.GattCharacteristic;
import higherground.lib.satpaqusbdriver.satpaq.enums.GattOperation;
import higherground.lib.satpaqusbdriver.satpaq.enums.GattService;

public final class SatPaqUsbEndpoints {
    final private static String TAG = "USB_ENDPTS";

    public static UsbMessage marshalNordicCommand(GattService service,
                                                  GattCharacteristic characteristic,
                                                  GattOperation operation,
                                                  byte[] command) {
        Objects.requireNonNull(service);
        Objects.requireNonNull(characteristic);
        Objects.requireNonNull(operation);

        HGNormalLog.trace(TAG, "marshalNordicCommand: %s %s %s",
                service.toString(), characteristic.toString(), operation.toString());

        int datalen = 0;
        if (command != null) datalen = command.length;

        // XXX - There needs to be a way to validate that the characteristic matches the service.
        if (characteristic.getService() != service) {
            HGNormalLog.error(TAG, "marshalNordicCommand: Service mismatch: %s vs %s (%s)",
                    service, characteristic.getService(), characteristic);

            return null;
        }

        int cmd_len = 3 + datalen;
        byte base_cmd[] = new byte[cmd_len];
        base_cmd[0] = service.getService();
        base_cmd[1] = characteristic.getCharacteristic();
        base_cmd[2] = operation.getCharacteristic();

        // If present, copy the data from the command.  Assume it is correct and necessary.
        if (command != null) {
            System.arraycopy(command, 0, base_cmd, 3, command.length);
        }

        String encoded = Base64.encodeToString(base_cmd, Base64.NO_WRAP);
        UsbMessage usbMessage = new UsbMessage("ncmd \"" + encoded + "\"\n", null);

        return usbMessage;
    }

    public static UsbMessage marshalTIVACommand(String message) {
        Objects.requireNonNull(message);
        HGNormalLog.trace(TAG, "marshalTIVACommand: %s", message);

        UsbMessage usbMessage = new UsbMessage(message + "\n", null);
        HGNormalLog.info(TAG, "marshalTIVACommand: Send Message: %s", usbMessage.toString());

        return usbMessage;
    }

    public static UsbMessage marshalSatelliteCommand(int msgID, byte[] data) {
        HGNormalLog.trace(TAG, "marshalSatelliteCommand: %d", msgID);
        if (data == null) {
            HGNormalLog.error(TAG, "marshalSatelliteCommand: null data");
            return null;
        }

        String command = String.format("txmsg %d %s\n", msgID, Base64.encodeToString(data, Base64.NO_WRAP));

        UsbMessage usbMessage = new UsbMessage(command, null);
        HGNormalLog.info(TAG, "MarshalSatelliteCommand: Send message: %s", usbMessage.toString());

        return usbMessage;
    }

    public static SatPaqEndpoint demarshalUSBData(String message) {
        HGNormalLog.trace(TAG, "demarshalUSBData");
        EndpointType eptype = EndpointType.Unknown;

        JSONObject jsonObject;

        try {
            jsonObject = new JSONObject(message);
        } catch (JSONException e) {
            HGNormalLog.error(TAG, "demarshalUSBData: JSON Exception: %s", e.toString());
            return null;
        }

        // Go through the JSON looking for a key
        Iterator i = jsonObject.keys();
        String key = null;
        while(i.hasNext()) {
            key = (String)i.next();
            HGNormalLog.info(TAG, "demarshalUSBData: JSON Key: %s", key);
            if(key.equals("ncmd")) {
                eptype = EndpointType.NordicGATT;

            // TODO: Satellite and PTStatus endpoints
            } else {
                eptype = EndpointType.TivaCommand;
            }
        }

        if((eptype == EndpointType.Unknown) || (key == null)) {
            // Unable to parse an endpoint type.
            HGNormalLog.error(TAG, "demarshalUSBData: Unable to determine endpoint type");
            return null;
        }

        JSONObject passobj;

        /*try {
            passobj = jsonObject.getJSONObject(key);
        } catch (JSONException e) {
            HGNormalLog.error(TAG, "demarshalUSBData: Unable to parse JSON value: %s", e.toString());
            return null;
        }*/

        switch(eptype) {
            case NordicGATT:
                String msg;
                try {
                    msg = jsonObject.getString(key);
                } catch (JSONException e) {
                    HGNormalLog.error(TAG, "demarshalUSBData: Unable to parse JSON value: %s", e.toString());
                    return null;
                }

                return parseNordicGatt(msg);
                //break;
            case Satellite:
                HGNormalLog.info(TAG, "demarshalUSBData: parse satellite not implemented.");
                return null;
                //break;
            case PTStatus:
                HGNormalLog.info(TAG, "demarshalUSBData: PTStatus not implemented.");
                return null;
                //break;
            case TivaCommand:
                SatPaqEndpoint ret = new SatPaqEndpoint();
                ret.type = EndpointType.TivaCommand;
                ret.data = message.getBytes();
                ret.usbMessage = message;

                return ret;
                //break;
            default:
                HGNormalLog.error(TAG, "demarshalUSBData: Unknown Endpoint Type");
                return null;
                //break;
        }

        // Control should not reach here, but if it does:
        //HGNormalLog.error(TAG, "demarshalUSBData: Unexpectedly reached end of function");
        //return null;
    }

    private static SatPaqEndpoint parseNordicGatt(String message) {
        HGNormalLog.trace(TAG, "parseNordicGatt: %s", message);
        SatPaqEndpoint ret = new SatPaqEndpoint();
        ret.type = EndpointType.NordicGATT;

        byte[] data;

        try {
            data = Base64.decode(message, Base64.DEFAULT);
        } catch (IllegalArgumentException e) {
            HGNormalLog.error(TAG, "parseNordicGatt: %s", e.toString());
            return null;
        }

        if(data.length < 3) {
            HGNormalLog.error(TAG, "parseNordicGatt: Data too short: %d bytes", data.length);
            return null;
        }

        //HGNormalLog.trace(TAG, "rxPDU:%s", HGStringUtil.getDataString(data, HGStringUtil.DataLogType.HEX_STR));
        HGNormalLog.info(TAG, "parseNordicGatt: Hex string: %s", byteToHexString(data));

        int servchar = (data[0] << 8) | data[1];

        // TODO: This probably should be converted to be part of the GattCharacteristic class
        switch (servchar) {
            case 0x0000: ret.characteristic = GattCharacteristic.ManufactureName; break;
            case 0x0001: ret.characteristic = GattCharacteristic.ModelNumber; break;
            case 0x0002: ret.characteristic = GattCharacteristic.SerialNumber; break;
            case 0x0003: ret.characteristic = GattCharacteristic.HardwareRevision; break;
            case 0x0004: ret.characteristic = GattCharacteristic.FirmwareRevision; break;
            case 0x0005: ret.characteristic = GattCharacteristic.SoftwareRevision; break;
            case 0x0006: ret.characteristic = GattCharacteristic.SystemID; break;
            case 0x0007: ret.characteristic = GattCharacteristic.BoardRevision; break;
            case 0x0100: ret.characteristic = GattCharacteristic.BatteryLevel; break;
            case 0x0101: ret.characteristic = GattCharacteristic.BatteryStatus; break;
            case 0x0200: ret.characteristic = GattCharacteristic.Command; break;
            case 0x0201: ret.characteristic = GattCharacteristic.Response; break;
            case 0x0202: ret.characteristic = GattCharacteristic.Status; break;
            case 0x0203: ret.characteristic = GattCharacteristic.RXData; break;
            case 0x0204: ret.characteristic = GattCharacteristic.TXData; break;
            default:
                HGNormalLog.error(TAG, "parseNordicGatt: Unknown service 0x%04x", servchar);
                return null;
        }

        ret.service = ret.characteristic.getService();

        switch (data[2]) {
            case 0x00: ret.operation = GattOperation.ReadProperty; break;
            case 0x01: ret.operation = GattOperation.WriteProperty; break;
            case 0x02: ret.operation = GattOperation.ReadValue; break;
            case 0x03: ret.operation = GattOperation.WriteValue; break;
            default:
                HGNormalLog.error(TAG, "parseNordicGatt: Unknown operation 0x02x", data[2]);
                return null;
        }

        // Copy out data if there are more than 3 bytes:
        if(data.length > 3) {
            ret.data = new byte[data.length - 3];
            System.arraycopy(data, 3, ret.data, 0, data.length - 3);
        } else {
            ret.data = new byte[0];
        }

        Gson gson = new Gson();
        HGNormalLog.info(TAG, "parseNordicGatt: returning: %s", gson.toJson(ret));

        return ret;
    }

    private static String byteToHexString(byte[] data) {
        StringBuilder sb = new StringBuilder();

        for(int i = 0; i < data.length; i++) {
            sb.append(String.format("%02X ", data[i]));
        }

        return sb.toString();
    }
}
