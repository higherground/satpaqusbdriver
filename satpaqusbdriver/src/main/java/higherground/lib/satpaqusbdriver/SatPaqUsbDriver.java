package higherground.lib.satpaqusbdriver;

import android.content.Context;
import android.util.Log;

import higherground.lib.logger.HGNormalLog;
import java.util.Hashtable;

public class SatPaqUsbDriver implements SerialListener {
    private static final String TAG = "USB_DVR";
    private final Hashtable<String, UsbDriverObserved> observers;

    private UsbSerialSocket socket = null;
    private Context context;
    private static SatPaqUsbDriver instance;

    private boolean ready = true;

    public static SatPaqUsbDriver getInstance(){
       if(instance == null) {
           instance = new SatPaqUsbDriver();
       }

       return instance;
    }

    private SatPaqUsbDriver() {
        HGNormalLog.trace(TAG, "SatPaqUsbDriver: constructor($context)");
        this.observers = new Hashtable<String, UsbDriverObserved>();
    }

    public void init(Context context) {
        socket = new UsbSerialSocket(context, this);
        this.context = context;
    }

    // Public Interface:
    public void usbConnect() {
        HGNormalLog.trace(TAG,"usbConnect");
        socket.connect();
    }
    public void usbDisconnect() {
        HGNormalLog.trace(TAG,"usbDisconnect");
        socket.disconnect();
    }
    public void usbSend(UsbMessage message) {
        HGNormalLog.trace(TAG,"usbSend: (%s)", message.usbMessage);
        // TODO: Turn this into a <s>real</s> queue.
        //while(!ready) {
        //    Thread.yield();
        //}
        message.usbRC = socket.write(message.usbMessage.getBytes());
        HGNormalLog.trace(TAG,"usbSend: Return code (%s)", message.usbRC.toString());

        for(String k: this.observers.keySet()) {
            UsbDriverObserved v = this.observers.get(k);
            if(v != null) v.onUsbSend(message);
        }
    }
    public void usbRegisterObservers(String observerID, UsbDriverObserved observers) {
        HGNormalLog.trace(TAG, "usbRegisterObservers: (%s)", observerID);
        this.observers.put(observerID, observers);
    }
    public void usbRemoveObservers(String observerID) {
        HGNormalLog.trace(TAG,"usbRemoveObservers: (%s)", observerID);
        this.observers.remove(observerID);
    }

    @Override
    public void onSerialConnect() {
        HGNormalLog.trace(TAG,"onSerialConnect");
        HGNormalLog.info(TAG, "USB Connected");

        // Always assume we're ready to send after connect.
        ready = true;

        for(String k: this.observers.keySet()) {
            UsbDriverObserved v = this.observers.get(k);
            if(v != null) v.onConnectChange(UsbConnectStatus.usbConnected);
        }
    }

    public void onSerialDisconnect() {
        HGNormalLog.trace(TAG,"onSerialDisconnect");
        HGNormalLog.info(TAG, "USB Disconnected");
        for(String k: this.observers.keySet()) {
            UsbDriverObserved v = this.observers.get(k);
            if(v != null) v.onConnectChange(UsbConnectStatus.usbDisconnected);
        }
    }

    @Override
    public void onSerialConnectError(Exception e) {
        HGNormalLog.trace(TAG,"onSerialConnectError");
        HGNormalLog.error(TAG, "USB Connect Failed: Exception: %s", e.toString());
        for(String k: this.observers.keySet()) {
            UsbDriverObserved v = this.observers.get(k);
            if(v != null) v.onConnectChange(UsbConnectStatus.usbConnectFailed);
        }
    }

    @Override
    public void onSerialRead(byte[] data) {
        HGNormalLog.trace(TAG,"onSerialRead: (%s)", new String(data));

        UsbMessage message = new UsbMessage();
        message.usbMessage = new String(data);

        for(String k: this.observers.keySet()) {
            UsbDriverObserved v = this.observers.get(k);
            if(v != null) v.onReceived(message);
        }

        // Look for a '>'
        if(message.usbMessage.contains(">")) {
            Log.e(TAG, "Ready to send next command.");
            ready = true;
        }
    }

    @Override
    public void onSerialIoError(Exception e) {
        HGNormalLog.trace(TAG,"onSerialIoError: closing");

        socket.disconnect();
    }
}
