package higherground.lib.satpaqusbdriver;



public interface UsbDriverObserved {
    void onUsbSend(UsbMessage message);
    void onReceived(UsbMessage message);
    void onConnectChange(UsbConnectStatus status);
}