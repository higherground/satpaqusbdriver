package higherground.lib.satpaqusbdriver;

public class UsbMessage {
    public String usbMessage;
    public UsbReturnCodes usbRC;

    public UsbMessage(){}
    public UsbMessage(String usbMessage,
                      UsbReturnCodes usbReturnCode){

        this.usbMessage = usbMessage;
        this.usbRC = usbReturnCode;
    }

    @Override
    public String toString() {
        String RC;
        if(this.usbRC == null) RC = "null";
        else RC = this.usbRC.toString();

        return new String(this.usbMessage + "(" + RC + ")");
    }
}

