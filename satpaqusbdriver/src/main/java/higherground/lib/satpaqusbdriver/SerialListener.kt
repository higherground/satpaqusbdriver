package higherground.lib.satpaqusbdriver

interface SerialListener {
  fun onSerialConnect()
  fun onSerialDisconnect()
  fun onSerialConnectError(e: Exception?)
  fun onSerialRead(data: ByteArray?)
  fun onSerialIoError(e: Exception?)
}