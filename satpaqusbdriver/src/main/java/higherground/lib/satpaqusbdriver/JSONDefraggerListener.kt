package higherground.lib.satpaqusbdriver

interface JSONDefraggerListener {
  fun onMessageSent(message: UsbMessage)
  fun onMessageReceived(message: String)
}