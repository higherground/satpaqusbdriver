package higherground.lib.satpaqusbdriver;

public enum UsbConnectStatus {
    usbDisconnected,
    usbConnecting,
    usbConnected,
    usbConnectFailed
}