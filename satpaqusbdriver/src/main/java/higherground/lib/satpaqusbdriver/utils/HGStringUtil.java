package higherground.lib.satpaqusbdriver.utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import higherground.lib.satpaqusbdriver.utils.HGBase64;
import higherground.lib.satpaqusbdriver.utils.HGHexCoder;

public class HGStringUtil {
    public enum DataLogType {
        HEX_STR,        // <STR>, <HEX>
        HEX_BASE64;     // hex: <HEX_STR>, base64: <Base64_STR>
    }

    public static String getDataString(byte[] data, DataLogType type) {
        String hexStr = HGHexCoder.toHex(data);
        String asciiStr = getAsciiString(data);
        String base64Str = HGBase64.encodeWebSafe(data);

        String result = null;

        switch (type) {
            case HEX_BASE64:
                result = String.format("hex: <%s>, base64: <%s>", hexStr, base64Str);
                break;
            case HEX_STR:
                result = String.format("<%s>, <%s>", hexStr, asciiStr);
                break;
        }

        return result;
    }

    public static String getAsciiString(byte[] data) {
        if(data == null || data.length == 0) {
            return "";
        }

        StringBuilder result = new StringBuilder();
        for(byte val : data) {
            if(val > 31 && val < 128)
                result.append(String.format("%c",val));
            else
                result.append(".");
        }
        return result.toString();
    }

    public static JSONObject getJSONObject(String string) {
        JSONObject object = null;
        try {
            object = new JSONObject(string);
        } catch (JSONException e) {
            object = null;
        }

        return object;
    }


    // -- Some useful string methods

    /**
     * is null or its length is 0 or it is made by space
     *
     * <pre>
     * isBlank(null) = true;
     * isBlank(&quot;&quot;) = true;
     * isBlank(&quot;  &quot;) = true;
     * isBlank(&quot;a&quot;) = false;
     * isBlank(&quot;a &quot;) = false;
     * isBlank(&quot; a&quot;) = false;
     * isBlank(&quot;a b&quot;) = false;
     * </pre>
     *
     * @param str
     * @return if string is null or its size is 0 or it is made by space, return true, else return false.
     */
    public static boolean isBlank(String str) {
        return (str == null || str.trim().length() == 0);
    }

    /**
     * is null or its length is 0
     *
     * <pre>
     * isEmpty(null) = true;
     * isEmpty(&quot;&quot;) = true;
     * isEmpty(&quot;  &quot;) = false;
     * </pre>
     *
     * @param str
     * @return if string is null or its size is 0, return true, else return false.
     */
    public static boolean isEmpty(CharSequence str) {
        return (str == null || str.length() == 0);
    }



    /**
     * get length of CharSequence
     *
     * <pre>
     * length(null) = 0;
     * length(\"\") = 0;
     * length(\"abc\") = 3;
     * </pre>
     *
     * @param str
     * @return if str is null or empty, return 0, else return {@link CharSequence#length()}.
     */
    public static int length(CharSequence str) {
        return str == null ? 0 : str.length();
    }

    /**
     * null Object to empty string
     *
     * <pre>
     * nullStrToEmpty(null) = &quot;&quot;;
     * nullStrToEmpty(&quot;&quot;) = &quot;&quot;;
     * nullStrToEmpty(&quot;aa&quot;) = &quot;aa&quot;;
     * </pre>
     *
     * @param str
     * @return
     */
    public static String nullStrToEmpty(Object str) {
        return (str == null ? "" : (str instanceof String ? (String)str : str.toString()));
    }

    /**
     * capitalize first letter
     *
     * <pre>
     * capitalizeFirstLetter(null)     =   null;
     * capitalizeFirstLetter("")       =   "";
     * capitalizeFirstLetter("2ab")    =   "2ab"
     * capitalizeFirstLetter("a")      =   "A"
     * capitalizeFirstLetter("ab")     =   "Ab"
     * capitalizeFirstLetter("Abc")    =   "Abc"
     * </pre>
     *
     * @param str
     * @return
     */
    public static String capitalizeFirstLetter(String str) {
        if (isEmpty(str)) {
            return str;
        }

        char c = str.charAt(0);
        return (!Character.isLetter(c) || Character.isUpperCase(c)) ? str : new StringBuilder(str.length())
                .append(Character.toUpperCase(c)).append(str.substring(1)).toString();
    }


    public static String array2String(Object[] values) {
        String result = "";

        for (Object value : values) {
            if (!result.isEmpty()) {
                result += ", ";
            }

            result += value.toString();
        }

        return result;
    }

    public static String list2String(List items) {
        Object[] array = items.toArray();
        return array2String(array);
    }
}
