package higherground.lib.satpaqusbdriver.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.regex.Pattern;

public class HGJsonUtil {
    private static String LOG_TAG = "JSON_UTIL";
    private static final Gson gson = new Gson();

    private HGJsonUtil() {
    }

    /**
     * Check wheter string is valid json or not
     *
     * @param jsonString input string for check
     * @return return the true or not
     */
    public static boolean isValidJson(String jsonString) {
        try {
            JsonParser parser = new JsonParser();
            JsonElement element = parser.parse(jsonString);
            if (element.isJsonNull() || !element.isJsonObject()) {
                return false;
            } else {
                return true;
            }
        } catch (JsonSyntaxException e) {
            return false;
        }
    }

    /**
     * Parse the first key from a JSON string.
     *
     * @param jsonString the JSON string to extract the first key from
     * @return First key of string or empty string is not a JSON string, or does not have a key.
     */
    public static String getFirstKey(String jsonString) {
        String firstKey = "";
        if (isValidJson(jsonString)) {
            // Json parsers don't necessarily guarantee the ordering of key-value pairs
            try {
                Scanner scanner = new Scanner(jsonString);
                scanner.useDelimiter("\"");
                // Skip the beginning '{'
                scanner.next();

                firstKey = scanner.next();
            } catch (InputMismatchException e ) {
            }
        }
        return firstKey;
    }

    public static JsonObject getJSONObject(String jsonString) {
        if (isValidJson(jsonString)) {
            JsonParser parser = new JsonParser();
            JsonObject object = parser.parse(jsonString).getAsJsonObject();
            return object;
        } else {
            return null;
        }
    }

    public static String getPrettyJsonString(JsonObject jsonObject) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(jsonObject);
    }


    public static boolean isJsonEmpty(JsonObject jsonObject) {
        return jsonObject.size() == 0;
    }
}
