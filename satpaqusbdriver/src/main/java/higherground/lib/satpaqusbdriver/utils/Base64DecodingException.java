package higherground.lib.satpaqusbdriver.utils;

public class Base64DecodingException extends Exception {
    private static final long serialVersionUID = -2574909128694204517L;

    public Base64DecodingException(Throwable cause) {
        super(cause);
    }

    public Base64DecodingException(String string) {
        super(string);
    }
}
