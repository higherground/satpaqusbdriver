package higherground.lib.satpaqusbdriver.utils

import java.nio.ByteBuffer
import java.nio.ByteOrder

class HGByteBuffer (bytes: ByteArray) {
    val bb = ByteBuffer.wrap(bytes)

    companion object {
        private const val N0_DIVISOR = 2.5
    }

    fun order(endian : ByteOrder) {
        bb.order(endian)
    }

    fun next8(): Byte {
        return bb.get()
    }

    fun nextU8(): Int {
        return bb.get().toInt() and 0xff
    }

    fun next16(): Int {
        return bb.short.toInt()
    }

    fun nextU16(): Int {
        return bb.short.toInt() and 0xffff
    }

    fun next32(): Int {
        return bb.int
    }

    @ExperimentalUnsignedTypes
    fun nextU64() : ULong {
        val num : Long = bb.getLong()
        return num.toULong()
    }

    fun parseN0FromByte(): Double {
        return next8().toDouble() / N0_DIVISOR
    }
}
