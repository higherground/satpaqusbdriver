package higherground.lib.satpaqusbdriver.utils
/**
 * Functions that implement simple bitwise operations such as AND
 * DD TODO: Is there a better Kotlin way???
 */
object HGBits {
    fun isSet(num: Number, mask: Number): Boolean {
        val a = num.toLong()
        val b = mask.toLong()
        return (a and b) != 0L
    }

    fun isSet(num: Number, mask: HasRawValue): Boolean {
        return isSet(num, mask.rawValue)
    }

    fun isSet(num: HasRawValue, mask: HasRawValue): Boolean {
        return isSet(num.rawValue, mask.rawValue)
    }
}

/** Represents a type that wraps an underlying raw value such as an bitmask enum. */
interface HasRawValue {
    val rawValue: Number
}
