package higherground.lib.satpaqusbdriver

import android.util.Log
import org.json.JSONException
import org.json.JSONObject
import kotlin.text.StringBuilder
import higherground.lib.logger.HGNormalLog


object JSONDefragger{
  val TAG:String = "JSONDefragger"

  internal var usbDriver: SatPaqUsbDriver = SatPaqUsbDriver.getInstance()
  internal lateinit var defragListener: JSONDefraggerListener
  internal lateinit var usbDriverObserver: UsbDriverObserved
  internal var jsonBuilder: StringBuilder = StringBuilder()

  fun registerUsbObserver(){
    this.usbDriverObserver = object : UsbDriverObserved {
      override fun onUsbSend(message: UsbMessage?) {
        usbSendHandler(message)
      }

      override fun onReceived(message: UsbMessage?) {
        usbReceiveHandler(message)
      }

      override fun onConnectChange(status: UsbConnectStatus?) {
        usbConnectionChangeHandler(status)
        // Reset the string on connect/disconnect
        jsonBuilder = StringBuilder()
      }
    }
    usbDriver.usbRegisterObservers(this.javaClass.name,
                                   this.usbDriverObserver)
  }

  init {
    registerUsbObserver()
  }

  private fun usbConnectionChangeHandler(status: UsbConnectStatus?) {
    HGNormalLog.trace(TAG, "usbConnectionChangeHandler:" + status.toString())
  }

  private fun usbReceiveHandler(message: UsbMessage?) {
    HGNormalLog.info(TAG, "usbReceiveHandler:" + message.toString())
    val strMessage = message?.usbMessage

    if(strMessage == null) {
      HGNormalLog.trace(TAG, "usbReceiveHandler: strMessage is null")
      return
    }

    // Short-circuit if there is no usable data.
    if((this.jsonBuilder.isEmpty()) && (strMessage.indexOf('{', 0, false) < 0)) {
      HGNormalLog.trace(TAG, "No JSON.\nDebug Message : %s", strMessage)
      return
    }

    this.jsonBuilder.append(strMessage)

    val st_idx = this.jsonBuilder.indexOf("{", 0, false)
    val end_idx = this.jsonBuilder.lastIndexOf('}')

    if((st_idx < 0) || (end_idx < 0)) {
      //TODO this needs to be handled
      HGNormalLog.trace(TAG, "Missing Brace: %s", strMessage)
      return
    }

    val candidate = this.jsonBuilder.substring(st_idx, end_idx + 1)
    HGNormalLog.trace(TAG, "usbReceiveHandler: Candidate string: $candidate")

    try {
      JSONObject(candidate)
    } catch (e: JSONException) {
      HGNormalLog.trace(TAG, "usbReceiveHandler: JSON Exception: $e")

      // Complete hack here to break out of unparseable text that looks vaguely like JSON:
      // TODO: Implement real lexical analyzer.
      var bracect = 0
      for(i in 0..candidate.length - 1) {
        if(candidate[i] == '{') bracect++
      }

      // XXX - This will be a problem if a command returns a fragmented response with multiple nested objects.
      if(bracect > 5) {
        HGNormalLog.error(TAG, "usbReceiveHandler: Dumping buffer because string is likely unusable.")
        this.jsonBuilder.clear()
      }

      return
    }

    // If we're here, we have valid JSON
    this.defragListener.onMessageReceived(candidate)
    this.jsonBuilder.clear()
  }

  private fun usbSendHandler(message: UsbMessage?) {
    // When a new message is sent, clear the jsonBuilder
    //this.jsonBuilder.clear()

    if (message != null) {
      HGNormalLog.trace(TAG, "usbSendHandler: $message")
      this.defragListener.onMessageSent(message)
    } else {
      HGNormalLog.trace(TAG, "usbSendHandler: null!")
    }
  }

  fun registerListener(listener: JSONDefraggerListener) {
    this.defragListener = listener
  }
}

