package higherground.lib.satpaqusbdriver

import higherground.lib.satpaqusbdriver.JSONDefragger.registerUsbObserver
import higherground.lib.satpaqusbdriver.UsbReturnCodes.OKAY
import junit.framework.Assert.assertTrue
import org.junit.After
import org.junit.Before
import org.junit.Ignore
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.times
import org.mockito.MockitoAnnotations


class JSONDefraggerTest {


  @Mock
  lateinit var mockDefragListener: JSONDefraggerListener
  @Mock
  lateinit var mockUsbDriver: SatPaqUsbDriver

  @Before
  fun setUp() {
    MockitoAnnotations.initMocks(this)
    JSONDefragger.defragListener = mockDefragListener
    JSONDefragger.usbDriver = mockUsbDriver
    registerUsbObserver()
  }

  @After
  fun tearDown() {
  }

  @Test
  fun testUsbDriverReceivesCorrectCommand() {
    val command = UsbMessage("Test Message", OKAY)

    JSONDefragger.usbDriverObserver.onUsbSend(command)

    Mockito.verify(mockDefragListener).onMessageReceived(command)
  }


  @Test
  fun testDefraggerParsesSimpleJsonCommandFromUsbDriver() {
    JSONDefragger.jsonBuilder.clear()

    val json = "{ \"test\" : \"Sample Json\"}"
    val command = UsbMessage("$json\n", OKAY)

    JSONDefragger.usbDriverObserver.onReceived(command)

    Mockito.verify(mockDefragListener).onMessageSent(json)
  }

  @Test
  fun testDefraggerParsesBeginningOfJSONAndDoesNotSend(){
    JSONDefragger.jsonBuilder.clear()

    val json = "Hi Bob\n{\"sver\":\n"
    val command = UsbMessage(json, OKAY)

    JSONDefragger.usbDriverObserver.onReceived(command)

    Mockito.verify(mockDefragListener, times(0)).onMessageSent("{\"sver\":")
    val retstr = JSONDefragger.jsonBuilder.toString()
    assertTrue(json.equals(retstr))
  }

  @Test
  fun testDefraggerParsesEndOfJSONAndSendsMessage(){
    JSONDefragger.jsonBuilder.clear()

    val jsonStart = "Hi Bob\n{\"sver\":"
    val jsonEnd = "\"versionStuff\"}\n"
    val fullJson = "{\"sver\":\"versionStuff\"}"
    val command1 = UsbMessage(jsonStart, OKAY)
    val command2 = UsbMessage(jsonEnd, OKAY)

    JSONDefragger.usbDriverObserver.onReceived(command1)
    JSONDefragger.usbDriverObserver.onReceived(command2)

    Mockito.verify(mockDefragListener, times(1)).onMessageSent(fullJson)
  }

  // Ignore two messages for now until we can determine if a rebuilt JSON message is acceptable.
  @Ignore @Test
  fun testTwoMessagesReceivedWithTwoFullJsonMessages(){
    val jsonStart = "Hi Bob\\n{\"sver\":\"versionStuff\"}\n{\"sinfo\":\"ssss"
    val jsonEnd = "sssfff\"}\n"
    val fullJson1 = "{\"sver\":\"versionStuff\"}\n"
    val fullJson2 = "{\"sinfo\":\"sssssssfff\"}\n"
    val command1 = UsbMessage(jsonStart, OKAY)
    val command2 = UsbMessage(jsonEnd, OKAY)

    JSONDefragger.usbDriverObserver.onReceived(command1)
    JSONDefragger.usbDriverObserver.onReceived(command2)

    Mockito.verify(mockDefragListener, times(2)).onMessageSent(ArgumentMatchers.anyString())
    Mockito.verify(mockDefragListener).onMessageSent(fullJson1)
    Mockito.verify(mockDefragListener).onMessageSent(fullJson2)
  }

  @Test
  fun testFragmentedNestedJSON() {
    JSONDefragger.jsonBuilder.clear()

    val part1 = "{"
    val part2 = "\""
    val part3 = "ue"
    val part4 = "cho\":{\"vers\":\"1.0\", \"msg\":\"test\"}}\nTrailing cruft"
    val assembled = "{\"uecho\":{\"vers\":\"1.0\", \"msg\":\"test\"}}"

    JSONDefragger.usbDriverObserver.onReceived(UsbMessage(part1,OKAY))
    JSONDefragger.usbDriverObserver.onReceived(UsbMessage(part2,OKAY))
    JSONDefragger.usbDriverObserver.onReceived(UsbMessage(part3,OKAY))
    JSONDefragger.usbDriverObserver.onReceived(UsbMessage(part4,OKAY))

    Mockito.verify(mockDefragListener, times(1)).onMessageSent(assembled)
  }

  @Test
  fun testMidSentenceNewline() {
    // A mid-sentence newline is not likely, but should be handled properly
    val part1 = "{\"uecho\":{\"sver\":\n"
    // An end-of-sentence newline IS likely, so our tests should do this in general
    val part2 = "\"version\"} }\n"
    // The assembled JSON SHOULD be removing trailing newlines, but keeping mid-sentence ones.
    val assembled = "{\"uecho\":{\"sver\":\n\"version\"} }"

    JSONDefragger.usbDriverObserver.onReceived(UsbMessage(part1,OKAY))
    JSONDefragger.usbDriverObserver.onReceived(UsbMessage(part2,OKAY))

    Mockito.verify(mockDefragListener, times(1)).onMessageSent(assembled)
  }
}