package higherground.lib.satpaqusbdriver;

import junit.framework.TestCase;

import org.junit.Test;

import higherground.lib.satpaqusbdriver.satpaq.SatPaq;
import higherground.lib.satpaqusbdriver.satpaq.enums.GattCharacteristic;
import higherground.lib.satpaqusbdriver.satpaq.enums.GattOperation;
import higherground.lib.satpaqusbdriver.satpaq.enums.GattService;

public class EndpointsMarshalTest extends TestCase {

    // No setup required as it is a static class
    protected void setUp() {

    }

    @Test
    public void testMarshalTIVA() {
        UsbMessage usbMessage = SatPaqUsbEndpoints.marshalTIVACommand("uecho \"test\"");
        assertEquals("uecho \"test\"\n", usbMessage.usbMessage);
    }

    @Test
    public void testNordicServiceMismatch() {
        UsbMessage usbMessage = SatPaqUsbEndpoints.marshalNordicCommand(
                GattService.DeviceInfo,
                GattCharacteristic.BatteryLevel,
                GattOperation.ReadValue,
                null);

        assertEquals(usbMessage, null);
    }

    @Test
    public void testMarshalNordicSerialNumber() {
        UsbMessage usbMessage = SatPaqUsbEndpoints.marshalNordicCommand(
                GattService.DeviceInfo,
                GattCharacteristic.SerialNumber,
                GattOperation.ReadValue,
                null);

        assertEquals("ncmd \"AAIC\"\n", usbMessage.usbMessage);
    }
}
