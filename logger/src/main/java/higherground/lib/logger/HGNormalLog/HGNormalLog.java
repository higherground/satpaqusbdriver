package higherground.lib.logger;

import android.util.Log;

public class HGNormalLog {
    public static void trace(String tag, String message) {
        Log.d(tag, message);
    }
    public static void trace(String tag, String format, Object... args) {
        trace(tag, String.format(format, args));
    }
    public static void info(String tag, String message) {
        Log.i(tag, message);
    }
    public static void info(String tag, String format, Object... args) {
        info(tag, String.format(format, args));
    }
    public static void error(String tag, String message) {
        Log.e(tag, message);
    }
    public static void error(String tag, String format, Object... args) {
        error(tag, String.format(format, args));
    }
}
