package higherground.lib.satpaqusbdriver.example

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.lifecycle.Observer
import butterknife.ButterKnife
import higherground.lib.satpaqusbdriver.SatPaqUsbDriver
import higherground.lib.logger.HGNormalLog
import higherground.lib.satpaqusbdriver.UsbMessage
import higherground.lib.satpaqusbdriver.example.ViewModels.SatPaqViewModel
import kotlinx.android.synthetic.main.activity_uecho_info.*
import java.lang.Exception
import java.security.spec.ECField

class UEchoInfoActivity : AppCompatActivity() {

    val driver = SatPaqUsbDriver.getInstance()
    var isRandom: Boolean = false
    var count: Int = 0
    var size: Int = 0
    var last_message: String = ""

    var current_count = 0
    var failures = 0
    var lastsend_time: Long = 0
    var total_elapsed: Long = 0

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_uecho_info)
        ButterKnife.bind(this)

        switch_random.setOnCheckedChangeListener { _, isChecked ->
            isRandom = isChecked
        }

        edit_count.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                try {
                    count = Integer.parseInt(p0.toString())
                } catch (e: Exception) {
                    count = 1
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })

        edit_size.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                try {
                    size = Integer.parseInt(s.toString())
                } catch (e: Exception) {
                    size = 1
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        btn_start.setOnClickListener { startUecho() }

        SatPaqViewModel.getUEchoInfo().observe(this, Observer {
            val received_time = System.currentTimeMillis()
            val elapsed = received_time - lastsend_time
            total_elapsed += elapsed

            if(last_message != it.data?.msg) {
                HGNormalLog.error("UEchoActivity", "Sent and received data don't match")
                failures = failures + 1
            }

            var avgtime: Double = -1.0
            if(current_count != 0) avgtime = total_elapsed.toDouble()/current_count.toDouble()

            val bps = last_message.length / (avgtime/1000.0)
            txt_curr_speed.text = "Average time : " + avgtime.toString() + "ms / " + "%.1f".format(bps) + " bytes/sec"
            txt_iteration_count.text = "Iteration Count : $current_count"
            txt_failures.text = "Failures: $failures"

            // Trigger the next iteration
            if(current_count < count) nextIteration()
        })
    }

    fun startUecho() {
        val dictionary: String = "x01x02x03x04x05x06x07x08x09x0Ax0Bx0Cx0Dx0Ex0F"
        val dictlen = dictionary.length
        val msgstr = StringBuilder()

        var reqsize = size

        while (reqsize > 0) {
            var chunk: Int

            if (reqsize > dictlen) chunk = dictlen
            else chunk = reqsize

            msgstr.append(dictionary.substring(0, chunk))
            reqsize -= chunk
        }

        last_message = msgstr.toString()

        current_count = 0
        failures = 0
        total_elapsed = 0

        nextIteration()
    }

    private fun nextIteration() {
        val msg = UsbMessage()

        msg.usbMessage = "uecho \"$last_message\"\n"
        lastsend_time = System.currentTimeMillis()
        driver.usbSend(msg)

        current_count++
    }

}
