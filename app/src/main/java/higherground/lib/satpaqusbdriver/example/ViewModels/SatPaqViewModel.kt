package higherground.lib.satpaqusbdriver.example.ViewModels

import android.content.Context
import android.view.ViewDebug.trace
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import higherground.lib.satpaqusbdriver.satpaq.operations.*
import higherground.lib.satpaqusbdriver.satpaq.SatPaq
import higherground.lib.satpaqusbdriver.satpaq.SatPaqCallbacks
import higherground.lib.logger.HGNormalLog


import higherground.lib.satpaqusbdriver.satpaq.SatPaqFactory

object SatPaqViewModel : ViewModel() {
    private const val LOG_TAG = "SatPaqVM"

    // TODO Replace with SatPaq Util in SpaceLinq App
    private var satPaq: SatPaq? = null

    private val ldConnectStatus = MutableLiveData<SPOperation>()
    fun getConnectStatus(): LiveData<SPOperation> {
        return ldConnectStatus
    }

    private val ldNordicInfo = MutableLiveData<NordicInfo>()
    fun getNordicInfo(): LiveData<NordicInfo> {
        return ldNordicInfo
    }

    private val ldTivaInfo = MutableLiveData<TivaInfo>()
    fun getTivaInfo(): LiveData<TivaInfo> {
        return ldTivaInfo
    }

    private val ldUEchoInfo = MutableLiveData<TivaUEcho>()
    fun getUEchoInfo(): LiveData<TivaUEcho> {
        return ldUEchoInfo
    }


    /**
     * Set the current SatPaq by its advertised name - normally the SatPaq serial number
     * Stores in settings and initialized the SPOperation Callback
     *
     * @param name The SatPaq Serial Number
     */
    fun selectSatPaq(name : String, context:Context) {
        //TODO Why can't I access the logger from here??
        HGNormalLog.info(LOG_TAG, "selectSatPaq=$name")

        // TODO Wait until integration with SpaceLinq
        //SettingsGeneral.getInstance().satPaqName = name

        // Register our callbacks
        // TODO For now just get it directly from the factory
        // val satPaq = SatPaqUtil.getSatPaqHelper()

        satPaq = SatPaqFactory.getSatPaqHelper(name, context)

        val observer = SPOperation(
                clientID = LOG_TAG, // Must be unique
                opType = SPOpType.ALL,
                callback = ::onSPOperation
        )

        SatPaqCallbacks.callbackRegister(observer)
    }

    private fun onSPOperation(op: SPOperation) {
        HGNormalLog.trace(LOG_TAG, "onSPOperation: opType=${op.opType}")

        return when (op.opType) {
            SPOpType.CONNECT_STATUS -> onConnectStatus(op)
            SPOpType.NORDIC_INFO -> onNordicInfo(op)
            SPOpType.TIVA_INFO -> onTivaInfo(op)
            SPOpType.TIVA_UECHO -> onTivaUEcho(op)
            else -> onNotSupportedYet(op)
        }
    }

    private fun onTivaInfo(op: SPOperation) {
        HGNormalLog.trace(LOG_TAG, "onTivaInfo: opType=${op.opType}")
        ldTivaInfo.postValue(op as TivaInfo)
    }

    private fun onTivaUEcho(op: SPOperation) {
        HGNormalLog.trace(LOG_TAG, "onTivaUEcho: opType=${op.opType}")
        ldUEchoInfo.postValue(op as TivaUEcho)
    }

    private fun onNotSupportedYet(op: SPOperation) {
        HGNormalLog.trace(LOG_TAG, "onNotSupportedYet: opType=${op.opType}")

        // TODO: Need more handlers
        assert(false)
    }

    private fun onConnectStatus(op: SPOperation) {
        HGNormalLog.trace(LOG_TAG, "onConnectStatus: opType=${op.opType}")

        ldConnectStatus.value = op
        if (op.result == SPResult.CONNECTED) {
            // This will cause NordicInfo and TivaInfo to get populated
            satPaq?.refresh()
        }
    }

    private fun onNordicInfo(op: SPOperation) {
        HGNormalLog.trace(LOG_TAG, "onNordicInfo: opType=${op.opType}")
        ldNordicInfo.postValue(op as NordicInfo)
    }

    /**
     * Start the process of connecting to a SatPaq
     *
     * @param context
     *        If non-null, the context to show a "SatPaq Connecting" dialog for.
     *        If null, no dialog is shown.
     */
    // Vince TODO Call this function when the connect button is pressed
    fun connectSatPaq(name: String, context: Context) {
        HGNormalLog.trace(LOG_TAG, "Connecting to Satpaq")

        satPaq = SatPaqFactory.getSatPaqHelper(name, context)
        if (satPaq != null) {
            satPaq!!.connectSatPaq()
        }
    }

    fun disconnectSatPaq() {
        if (satPaq != null) {
            satPaq!!.disconnectSatPaq()
        }
    }
}
