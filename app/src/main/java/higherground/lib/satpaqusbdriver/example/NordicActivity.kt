package higherground.lib.satpaqusbdriver.example

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import higherground.lib.satpaqusbdriver.example.ViewModels.SatPaqViewModel
import kotlinx.android.synthetic.main.activity_nordic.*
import kotlinx.android.synthetic.main.activity_tiva_info.*

class NordicActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nordic)
        SatPaqViewModel.getNordicInfo().observe(this, Observer {
            txt_nordic_advertised_name.text = "Advertised Name : " + it.advertisedName
            txt_nordic_name.text = "Name : " + it.name
            txt_nordic_battery_level.text = "Battery Level : " + it.batteryLevel
            txt_nordic_board_version.text = "Board Version : " + it.boardRevision
            txt_nordic_bootloader_version.text = "BootLoader Version : " + it.bootloaderRevision
            txt_nordic_firmware_version.text = "Firmware Version: " + it.firmwareRevision
            txt_nordic_hardware_version.text = "Hardware Version: " + it.hardwareRevision
            txt_nordic_jerk.text = "Jerk : " + it.jerk
            txt_nordic_sn.text = "Serial Number : " + it.serialNumber
            txt_nordic_manufacturer_version.text = "Manufcaturer Version : " + it.manufactureName
            txt_nordic_model_name.text = it.modelNumber
            txt_nordic_status.text = it.status.toString()
            txt_nordic_system_id.text = it.systemID.toString()
            txt_nordic_temp.text = it.temperature.toString()
        })
    }

}
