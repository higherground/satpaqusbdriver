package higherground.lib.satpaqusbdriver.example

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.ViewDebug.trace
import androidx.lifecycle.Observer
import higherground.lib.satpaqusbdriver.example.ViewModels.SatPaqViewModel
import higherground.lib.logger.HGNormalLog
import kotlinx.android.synthetic.main.activity_tiva_info.*

class TivaInfoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tiva_info)
        SatPaqViewModel.getTivaInfo().observe(this, Observer {
            Log.e("TivaInfoActivity", "observer activity")
            txt_tiva_hw.text = "Hardware version: " + it.data?.hardwareVersion
            txt_tiva_id.text = it.data?.identifier
            txt_tiva_sn.text = it.data?.serialNumber
            txt_detail_version.text = it.data?.firmware?.version
            txt_detail_built.text = it.data?.firmware?.built
            txt_detail_description.text = it.data?.firmware?.description
        })
    }
}
