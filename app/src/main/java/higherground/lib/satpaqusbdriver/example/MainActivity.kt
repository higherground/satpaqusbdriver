package higherground.lib.satpaqusbdriver.example

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import butterknife.ButterKnife
import butterknife.OnClick
import com.google.gson.GsonBuilder
import higherground.lib.logger.HGNormalLog
import higherground.lib.satpaqusbdriver.*
import higherground.lib.satpaqusbdriver.example.ViewModels.SatPaqViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), UsbDriverObserved {
    private var driver: SatPaqUsbDriver = SatPaqUsbDriver.getInstance()
    private var isConnected = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ButterKnife.bind(this)
        // Open up the driver and register myself as the handler
        // TODO: This init should probably be handled by the Factory
        driver.init(applicationContext)
        driver.usbRegisterObservers("Main", this)
        driver.usbRegisterObservers(MAIN_ACTIVITY_OBSERVER, this)
        SatPaqViewModel.getConnectStatus().observe(this, Observer {
            btn_connect_toggle.text = it.result.name
        })

        SatPaqViewModel.getTivaInfo().observe(this, Observer {
            Log.e("nothis", "observer: " + it.data)
            text_hv.text = it.SVerData().hardwareVersion
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        driver.usbRemoveObservers(MAIN_ACTIVITY_OBSERVER)
    }

    @OnClick(R.id.main_send)
    fun sendUEcho() {
        startActivity(Intent(this, UEchoInfoActivity::class.java))
    }

    @OnClick(R.id.btn_connect_toggle)
    fun connectionToggleClick() {
        toggleConnection()
    }

    @OnClick(R.id.btn_tiva)
    fun openTivaInfo() {
        startActivity(Intent(this, TivaInfoActivity::class.java))
    }

    @OnClick(R.id.btn_nordic)
    fun openNordicInfo() {
        startActivity(Intent(this, NordicActivity::class.java))
    }

    private fun toggleConnection() {
        isConnected = if (isConnected) {
            SatPaqViewModel.disconnectSatPaq()
            false
        } else {
            SatPaqViewModel.connectSatPaq("USB", this)
            SatPaqViewModel.selectSatPaq("USB", this)
            true
        }
    }

    //region JSONDefraggerListener
    fun onMessageSent(msg: UsbMessage) {
        HGNormalLog.trace(TAG, "onMessageSent: $msg")
        val ep = SatPaqUsbEndpoints.demarshalUSBData(msg.usbMessage)
        val gson = GsonBuilder().setPrettyPrinting().create()
        HGNormalLog.info(TAG, "onMessageSend: demarshaled: %s", gson.toJson(ep))
    }

    fun onMessageReceived(message: String) {
        HGNormalLog.trace(TAG, "onMessageReceived: $message")
    }
    //endregion

    //region UsbDriverObserved
    override fun onUsbSend(msg: UsbMessage) {
        HGNormalLog.trace(TAG, "onUsbSend: " + msg.usbMessage + " // " + msg.usbRC.toString())
    }

    override fun onReceived(msg: UsbMessage) {
        HGNormalLog.trace(TAG, "onUsbReceived: " + msg.usbMessage)
    }

    override fun onConnectChange(status: UsbConnectStatus) {
        HGNormalLog.trace(TAG, "onConnectChange: $status")
    }
    //endregion

    companion object {
        private const val TAG = "MainActivity"
        const val MAIN_ACTIVITY_OBSERVER = "Main"
    }
}